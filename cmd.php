<?php

$container = require __DIR__ . '/app/bootstrap.php';

$application = $container->getByType(\Kdyby\Console\Application::class);
$application->run();
