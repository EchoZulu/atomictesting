import $ from 'jquery';
import './tools/svg4everybody';
// import { MQ } from './tools/MQ';

// Components
// import * as fontFaceObserver from './components/fontFaceObserver';
import * as cookie from './components/cookie';
import * as tabs from './components/tabs';
import * as carousel from './components/carousel';
import * as slider from './components/slider';
import * as filter from './components/filter';
// import * as ajaxChange from './components/ajaxChange';
// import * as ajaxFormFile from './components/ajaxFormFile';
import * as ajaxForm from './components/ajaxForm';
import * as antispam from './components/antispam';
import * as matchHeight from './components/matchHeight';
import * as copyText from './components/copyText';
import * as autoSubmit from './components/autoSubmit';
import * as fancybox from './components/fancybox';
import * as search from './components/search';
import * as geoCookie from './components/geoCookie';
import * as mobileTab from './components/mobileTab';
import * as filterOpen from './components/filterOpen';
import * as modernizrFix from './components/modernizrFix';
import * as elementClickable from './components/elementClickable';
import * as header from './components/header';
import * as starRating from './components/starRating';
import * as inpCount from './components/inpCount';
import * as linkSlide from './components/linkSlide';
import * as top from './components/top';
import * as close from './components/close';
import * as toggleClass from './components/toggleClass';
import * as toggleChecked from './components/toggleChecked';
import * as formOpen from './components/formOpen';
import * as showPassword from './components/showPassword';
import * as transportPayment from './components/transportPayment';
import * as toggle from './components/toggle';
import * as compare from './components/compare';
import * as filterBrands from './components/filterBrands';
import * as facebookShare from './components/facebookShare';
import * as headroom from './components/headroom';
import * as mobileMenu from './components/mobileMenu';
import * as dateTime from './components/dateTime';
import * as validate from './components/validate';
import * as eTarget from './components/eTarget';
import * as removeAddress from './components/removeAddress';
import * as nanoScroller from './components/nanoScroller';
import * as fixed from './components/fixed';
import * as menuSection from './components/menuSection';
import * as radioDeselect from './components/radioDeselect';
import * as formSubmit from './components/formSubmit';
import * as agree from './components/agree';
import * as ajaxLink from './components/ajaxLink';
import * as datalayer from './components/datalayer';
import * as inpDisable from './components/inpDisable';
import * as customSelect from './components/customSelect';
import * as tooltip from './components/tooltip';
import * as ouibounce from './components/ouibounce';
import * as lozad from './components/lozad';
import * as installment from './components/installment';
import * as step1AjaxReload from './components/step1AjaxReload';
import * as openLink from './components/openLink';


const componentsload = [
	lozad,
	antispam,
	dateTime,
	nanoScroller,
	slider,
	carousel,
	matchHeight,
	inpDisable,
	toggleClass,
	customSelect,
];
const components = [
	filter,
	// fontFaceObserver,
	ajaxLink,
	datalayer,
	step1AjaxReload,
	cookie,
	tabs,
	validate,
	geoCookie,
	copyText,
	autoSubmit,
	search,
	fancybox,
	ajaxForm,
	mobileTab,
	filterOpen,
	modernizrFix,
	elementClickable,
	header,
	starRating,
	inpCount,
	linkSlide,
	top,
	close,
	toggleChecked,
	formOpen,
	showPassword,
	transportPayment,
	toggle,
	compare,
	filterBrands,
	facebookShare,
	headroom,
	mobileMenu,
	eTarget,
	removeAddress,
	fixed,
	menuSection,
	radioDeselect,
	formSubmit,
	agree,
	tooltip,
	ouibounce,
	installment,
	openLink,
].concat( componentsload );

window.App = {
	run() {
		// MQ('desktopMin');

		var $target = $(document);
		components.forEach((component) => component.init( $target ));



		$(document)
			.on('contentload', function(event) {
				var $target = $(event.target);
				componentsload.forEach((component) => component.init( $target ));
			});
	},

	initComponent(component) {
		return component.init();
	},
};
