let cache = {};

export default (src) => {
	if (cache[src]) return cache[src];

	cache[src] = new Promise((resolve, reject) => {
		const s = document.createElement('script');
		s.type = 'text/javascript';
		s.async = true;
		s.onload = resolve;
		s.onerror = reject;
		s.src = src;
		document.head.appendChild(s);
	});

	return cache[src];
};
