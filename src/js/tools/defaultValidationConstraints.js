export default {
	email: {
		presence: {
			message: '^E-mail je povinný.',
		},
		email: {
			message: '^Fíha, zadali jste nesprávný e-mail.',
		},
	},
	password: {
		presence: {
			message: '^Heslo musí být vyplněné.',
		},
	},
	passwordVerify: {
		presence: {
			message: '^Hesla se neshodují.',
		},
		equality: {
			attribute: 'password',
			message: '^Hesla se neshodují.',
		},
	},
	name: {
		presence: {
			message: '^Jméno a příjmení je povinné.',
		},
	},
	dName: {
		presence: {
			message: '^Jméno a příjmení je povinné.',
		},
		format: {
			pattern: '[a-ž]+([ ]?)[a-ž]+',
			flags: 'i',
			message: '^Jméno nebo příjmení obsahuje nepovolené znaky.',
		},
	},
	ca_name: {
		presence: {
			message: '^Jméno a příjmení je povinné.',
		},
		format: {
			pattern: '[a-ž]+([ ]?)[a-ž]+',
			flags: 'i',
			message: '^Jméno nebo příjmení obsahuje nepovolené znaky.',
		},
	},
	street: {
		presence: {
			message: '^Ulice a číslo popisné je povinné.',
		},
	},
	dStreet: {
		presence: {
			message: '^Ulice a číslo popisné je povinné.',
		},
	},
	city: {
		presence: {
			message: '^Město je povinné.',
		},
	},
	dCity: {
		presence: {
			message: '^Město je povinné.',
		},
	},
	pin: {
		presence: {
			message: '^Rodné číslo je povinné, kvůli přikoupenému pojištění.',
		},
		format: {
			pattern: '\\d{6}([/]?)\\d{3,4}',
			message: '^Neplatný formát rodného čísla.',
		},

	},
	zip: {
		presence: {
			message: '^PSČ je povinné.',
		},
		format: {
			pattern: '\\d{3}([ ]?)\\d{2}',
			message: '^PSČ musí obsahovat 5 číslic a být ve formátu xxxxx nebo xxx xx.',
		},
	},
	dZip: {
		presence: {
			message: '^PSČ je povinné.',
		},
		format: {
			pattern: '\\d{3}([ ]?)\\d{2}',
			message: '^PSČ musí obsahovat 5 číslic a být ve formátu xxxxx nebo xxx xx.',
		},
	},
	phone: {
		presence: {
			message: '^Telefonní číslo je povinné.',
		},
		format: {
			pattern: '/?([0-9]{3})([ ]?)([0-9]{3})([ ]?)([0-9]{3})',
			message: '^Zadali jste nesprávný formát telefonního čísla. Upravte jej na 602xxxxxx nebo 602 xxx xxx.',
		},
	},
	phone2: {
		presence: {
			message: '^Telefonní číslo je povinné.',
		},

	},
	state: {
		presence: {
			message: '^Stát je povinné.',
		},
	},
	dState: {
		presence: {
			message: '^Stát je povinné.',
		},
	},
	// rating: {
	// 	presence: {
	// 		message: '^Hodnocení je povinné',
	// 	},
	// },

	presence: {
		presence: {
			message: '^Toto pole je povinné.',
		},
	},
	company: {
		presence: {
			message: '^Název společnosti je povinný.',
		},
	},
	ic: {
		format: {
			pattern: '\\d{0,8}',
			message: '^IČ má nesprávný formát.',
		},
		presence: {
			message: '^IČ je povinné.',
		},
	},
	dic: {
		presence: {
			message: '^DIČ je povinné.',
		},
	},
	note: {
		presence: {
			message: 'Zpráva je povinná.',
		},
	},
	agree: {
		presence: {
			message: '',
		},
	},
	theme: {
		presence: {
			message: '^Vyberte téma, kterého se dotaz týká.',
		},
	},
	message: {
		presence: {
			message: '^Zpráva je povinná',
		},
	},

};
