import loadScript from './loadScript';

let api = null;


export default () => {
	if (api) return api;

	api = new Promise((resolve, reject) => {

		window.onGmapsAPIReady = () => {
			loadScript('/static/js/infobox.js').then(() => {
				resolve();
			});
			delete window.onGmapsAPIReady;
		};

		loadScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyCPgEEHA3zF1e5lT9bN_cPIOR1SLKtsQkc&callback=onGmapsAPIReady')
			.catch(reject);
	});

	return api;
};
