(function(sk, $, undefined){

	sk.widgets.SuggestMenu = function(element, Suggest, options)
	{
		this.$element = element.jquery ? element : $(element);
		this.Suggest = Suggest;

		this.options = $.extend({
			item: 'li',
			selectedClass: 'is-selected'
		}, options);

		this.$items = $([]);
		this.selectedIndex = 0;
		this.size = 0;
		this.isOpen = false;
	};

	// prototype
	var _fn = sk.widgets.SuggestMenu.prototype;

	_fn.init = function()
	{
		if(!this.$element.length || this.Suggest === undefined)
		{
			return this;
		}

		this.$element
			.hide()
			.on('mouseenter', this.options.item, $.proxy(this.handleMouseEnter, this));
			// .on('click', this.options.item, $.proxy(this.handleClick, this));

		$(this.Suggest)
				.on('suggeststart', $.proxy(this.handleSuggestStart, this))
				.on('suggestend', $.proxy(this.handleSuggestEnd, this))
				.on('beforeSend', $.proxy(this.handleBeforeSend, this))
				.on('success', $.proxy(this.handleSuccess, this))
				.on('typestart', $.proxy(this.handleTypeStart, this))
				//.on('typeend', $.proxy(this.handleTypeEnd, this))
				.on('typeclear', $.proxy(this.handleTypeClear, this));

		this.Suggest.init();

		return this;
	};

	_fn.destroy = function()
	{
		if(!this.$element.length || this.Suggest === undefined)
		{
			return this;
		}

		this.$element
			.hide()
			.empty()
			.off('mouseenter', this.options.item, this.handleMouseEnter)
			.off('click', this.options.item, this.handleMouseEnter);

		$(this.Suggest)
			.off('suggeststart', this.handleSuggestStart)
			.off('suggestend', this.handleSuggestEnd)
			.off('success', this.handleSuccess)
			.off('typestart', this.handleTypeStart)
			.off('typeclear', this.handleTypeClear);

		this.Suggest.destroy();
	};

	_fn.open = function()
	{
		if(!this.isOpen && !this.$element.is(':empty'))
		{
			$(this).trigger('beforeopen');

			this.$element.fadeIn(250, $.proxy(function()
			{
				$(this).trigger('afteropen');

			}, this));

			this.isOpen = true;
		}
	};

	_fn.hide = function(fastHide)
	{
		if(this.isOpen)
		{
			$(this).trigger('beforehide');

			if(fastHide)
			{
				this.$element.hide();
				$(this).trigger('afterhide');
			}
			else
			{
				this.$element.fadeOut(250, $.proxy(function()
				{
					$(this).trigger('afterhide');

				}, this));
			}

			this.isOpen = false;
		}
	};

	_fn.handleSuggestStart = function(e)
	{
		if(this.Suggest.isResult)
		{
			this.clearIndex();
			this.open();
		}
	};

	_fn.handleSuggestEnd = function(e)
	{
		// odděleno zkrz vyhledávání bez focosu, u menu nemá smysl
		this.Suggest.typeTimer = clearTimeout(this.Suggest.typeTimer);
		this.Suggest.ajaxAbort();
		this.Suggest.$input.removeClass( this.Suggest.options.loadingClass );

		this.hide();
	};

	_fn.handleSuccess = function(e, respond)
	{
		var contentEvent = jQuery.Event('content');
		$(this).trigger(contentEvent, [respond]);

		if(!contentEvent.isDefaultPrevented())
		{
			this.$element.html(respond);
		}

		this.$items = this.$element.find( this.options.item );
		this.size = this.$items.length + 1;

		this.clearIndex();

		if( this.$element.is(':empty') )
		{
			this.hide(true);
		}
		else
		{
			this.open();
		}

		this.$element.trigger('contentload');
	};


	_fn.handleTypeStart = function(e)
	{
		if(e.which === 38)
		{
			this.handleArrUp(e);
		}
		else if(e.which === 40)
		{
			this.handleArrDown(e);
		}
		else if(e.which === 13 && this.selectedIndex)
		{
			this.handleEnter(e);
		}
	};

	_fn.handleTypeClear = function(e)
	{
		this.hide();
		this.clearIndex();
	};

	_fn.handleMouseEnter = function(e)
	{
		// if(this.isOpen)
		// {
		// 	// console.log(e.currentTarget)
		// 	// console.log( $(e.currentTarget) )
		// 	// console.log( $(e.currentTarget).index() )
		// 	// console.log( $(e.currentTarget).index( $('.f-search__item') ) )
		// 	this.setIndex( $(e.currentTarget).index()+1, 'enter' );
		// }
	};

	_fn.setIndex = function(index, type)
	{
		var o = this.options;

		if(index !== this.selectedIndex)
		{
			this.$items.removeClass( o.selectedClass );

			if(index)
			{
				this.$items.eq( index-1 ).addClass( o.selectedClass );
			}

			// Scroll to invisible item if keys
			if(index && type !== 'enter')
			{
				var scroll = this.$element.scrollTop();
				var top = scroll + this.$items.eq( index-1 ).position().top;
				var height = this.$element.height();

				if( top < scroll ||  top > scroll + height)
				{
					this.$element.scrollTop( top );
				}
			}

			this.selectedIndex = index;

			$(this).trigger('menuchange');
		}
	};
	_fn.clearIndex = function()
	{
		this.$items.removeClass( this.options.selectedClass );
		this.selectedIndex = 0;
	}

	_fn.handleArrUp = function(e)
	{
		this.setIndex( (this.selectedIndex - 1 + this.size) % this.size, 'up' );
		e.preventDefault();
	};

	_fn.handleArrDown = function(e)
	{
		this.setIndex( (this.selectedIndex + 1) % this.size, 'down' );
		e.preventDefault();
	};

	_fn.handleEnter = function(e)
	{
		e.preventDefault();

		$(this).trigger('menuselect');
	};
	_fn.handleClick = function(e)
	{
		e.preventDefault();

		this.setIndex( $(e.currentTarget).index()+1, 'click' );

		$(this).trigger('menuselect');
	};

})(sk, jQuery);
