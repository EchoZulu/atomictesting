(function(sk, $) {

	sk.widgets.Suggest = function(element, options)
	{
		this.$input = element.jquery ? element : $(element);

		this.options = $.extend({
			minLength: 2,
			typeInterval: 500, //ms
			url: '/',
			loadingClass: 'loading',
			typingClass: 'typing',
		}, options);

		this.isResult = false;
		this.typeTimer = null;
		this.searchTerm = '';
		this.ajax = [];
	};

	// prototype
	var _fn = sk.widgets.Suggest.prototype;


	// prototype methods
	_fn.init = function()
	{
		this.$input
			.attr('autocomplete', 'off')
			.on('keydown', $.proxy(this.keydown, this))
			.on('keypress', $.proxy(this.keypress, this))
			.on('focus', $.proxy(this.focus, this))
			.on('blur', $.proxy(this.blur, this));

		return this;
	};

	_fn.destroy = function()
	{
		this.$input
			.off('keydown', this.keydown)
			.off('keypress', this.keypress)
			.off('focus', this.focus)
			.off('blur', this.blur);

		return this;
	};

	_fn.focus = function()
	{
		$(this).trigger('suggeststart');

		this.type();
	};
	_fn.blur = function()
	{
		$(this).trigger('suggestend');

		this.$input.removeClass(this.options.typingClass);
	};

	_fn.keydown = function(e)
	{
		var that = this;
		var o = this.options;

		this.typeTimer = clearTimeout(this.typeTimer);
		this.typeTimer = setTimeout(function() {that.type();}, o.typeInterval);

		var typeEvent = jQuery.Event('typestart', {
			which: e.which,
		});
		$(this).trigger(typeEvent);

		if ( typeEvent.isDefaultPrevented() )
		{
			e.preventDefault();
		}
		else
		{
			this.$input.addClass(this.options.typingClass);
		}
	};
	_fn.keypress = function(e)
	{
		$(this).trigger('typing');

		if ( this.$input.val() !== this.searchTerm )
		{
			this.ajaxAbort();
		}
	};

	_fn.ajaxRemove = function(instance)
	{
		var arr = this.ajax;

		for (var i = 0, l = arr.length; i < l; i++) {
			if (arr[i] === instance) {
				break;
			}
		}

		arr.splice(i, 1);
	};

	_fn.ajaxAbort = function()
	{
		var arr = this.ajax;

		for (var i = 0, l = arr.length; i < l; i++) {
			arr[i].abort();
		}
	};

	_fn.type = function()
	{
		var value = this.$input.val();
		var o = this.options;

		if (value !== this.searchTerm)
		{
			this.searchTerm = value;

			if (value.length >= o.minLength)
			{
				this.ajaxAbort();

				// Sestavení dat k poslání na server
				var data = {};
				data[ this.$input.attr('name') ] = value;

				this.ajax.push(
					$.ajax({
						type: 'GET',
						url: o.url,
						data: data,
						beforeSend: $.proxy(function(jqXHR, settings)
						{
							var beforeSendEvent = jQuery.Event('beforeSend');
							$(this).trigger(beforeSendEvent, [jqXHR, settings]);

							if (beforeSendEvent.isDefaultPrevented())
							{
								return false;
							}

							this.$input.addClass(o.loadingClass);

						}, this),
						success: $.proxy(function(respond, textStatus, jqXHR)
						{
							this.ajaxRemove(jqXHR);

							var successEvent = jQuery.Event('success');
							$(this).trigger(successEvent, [respond, textStatus, jqXHR]);

							this.$input.removeClass(o.loadingClass);
							this.isResult = true;
						}, this),
						error: $.proxy(function(jqXHR, textStatus, errorThrown)
						{
							this.ajaxRemove(jqXHR);
							this.$input.removeClass(o.loadingClass);
						}, this),
					}),
				);
			}
			else if (this.isResult)
			{
				this.isResult = false;

				this.ajaxAbort();

				$(this).trigger('typeclear');
			}
			else
			{
				$(this).trigger('typeend');
			}
		}
		else
		{
			$(this).trigger('typeend');
		}

		this.$input.removeClass(o.typingClass);
	};

	_fn.clear = function()
	{
		this.$input.val('');
		this.typeTimer = clearTimeout(this.typeTimer);
		this.type();
	};

})(sk, jQuery);
