import $ from 'jquery';


export const init = () => {
	$(document)
		.on('click', '.js-show-password', function(e) {
			e.preventDefault();

			var $this = $(this),
				$inp = $this.parent().find('input');

			if ($inp.attr('type') == 'password') {
				$inp.attr('type', 'text');
			} else {
				$inp.attr('type', 'password');
			}
		});
};
