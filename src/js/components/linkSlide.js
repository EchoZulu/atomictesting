import $ from 'jquery';

export const init = ($target) => {

	$(document)
		.on('click', '.js-link-slide', function(e) {
			e.preventDefault();
			var $this = $(this),
				extraOffset = 0,
				elOffset = $( $(this).attr('href') ).offset().top,
				windowOffset = $(window).scrollTop(),
				scrollTop = 0;


			// if fixed bar exists
			if ($this.closest('.js-fixed__target').length) {
				extraOffset += $this.closest('.js-fixed__target').height();
			}


			if ( elOffset < windowOffset) { // SCROLL UP
				if (windowOffset - elOffset > 300) { // headroom will show
					scrollTop = elOffset - $target.find('.headroom').height() - extraOffset;
				} else if ( $target.find('.headroom:visible') ) { // headroom is visible
					scrollTop = elOffset - $target.find('.headroom').height() - extraOffset;
				} else { // no headroom
					scrollTop = elOffset - extraOffset;
				}
			} else { // SCROLL DOWN
				if (elOffset - windowOffset > 300 && $target.find('.headroom:visible').length) { // headroom is closing
					scrollTop = elOffset - extraOffset;
				} else if ( $target.find('.headroom:visible') ) { // headroom is NOT closing
					scrollTop = elOffset - $target.find('.headroom').height() - extraOffset;
				} else { // no headroom
					scrollTop = elOffset - extraOffset;
				}
			}
			$('body, html').animate({
				scrollTop: scrollTop,
			}, 500, 'linear');

		});
};
