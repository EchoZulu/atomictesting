import $ from 'jquery';


export const init = () => {
	const $document = $(document);
	const selectors = {
		TRIGGER: '[data-toggle-checked]',
	};
	const classes = {
		IS_CHECKED: 'is-checked',
	};

	const handleClick = (event) => {
		const $trigger = $(event.currentTarget);
		const $parent = $trigger.closest('li');
		const name = $trigger.attr('name');

		$('input[name="'+name+'"]').closest('li').filter('.'+classes.IS_CHECKED).removeClass(classes.IS_CHECKED);
		$parent.toggleClass(classes.IS_CHECKED);
	};

	$document
		.on('click', selectors.TRIGGER, handleClick);
};
