import $ from 'jquery';
import '@fancyapps/fancybox';

export const init = ($target, lang = 'cs') => {
	const DEFAULTS = {
		lang,
		i18n: {
			cs: {
				CLOSE: 'Zavřít',
				NEXT: 'Následující',
				PREV: 'Předchozí',
				ERROR: 'Požadovaný obsah se bohužel nepodařilo načíst. <br/> Zkuste to prosím později.',
				PLAY_START: 'Spustit slideshow',
				PLAY_STOP: 'Zastavit slideshow',
				FULL_SCREEN: 'Celá obrazovka',
				THUMBS: 'Náhledy',
				DOWNLOAD: 'Stáhnout',
				SHARE: 'Sdílet',
				ZOOM: 'Zoom',
			},
		},
		touch: false,
		beforeShow: function() {
			$('body').css({'overflow-y':'hidden'});
		},
		afterClose: function() {
			$('body').css({'overflow-y':'visible'});
		},
		afterLoad: function(e, el) {
			$(this)[0].$content.trigger('contentload');

			// fix iOS overflow hidden & half-visible absolute btn problem
			if (el.$slide.hasClass('fancybox-slide--iframe')) {
				setTimeout(function() {
					var $btn = el.$content.find('.fancybox-button');
					el.$slide.append($btn);

					$btn.css('top', ($(window).height() - el.$content.height()) / 2 > 15 ? ($(window).height() - el.$content.height()) / 2 : 15 ); // offset().top not working
					$btn.css('right', el.$content.offset().left);
				}, 1);
				$(window).on('resize', function() {
					var $btn = el.$slide.find('.fancybox-button');
					$btn.css('top', ($(window).height() - el.$content.height()) / 2 > 15 ? ($(window).height() - el.$content.height()) / 2 : 15 ); // offset().top not working
					$btn.css('right', el.$content.offset().left);
				});
			}

		},
	};
	$.extend(true, $.fancybox.defaults, DEFAULTS);

	// AFTER LOAD
	$target.find('.js-fancybox-open-after-load').each(function() {
		var $this = $(this);

		setTimeout(function() {
			$.fancybox.open({
				src: '#'+$this.attr('id'),
				type: 'inline',
				afterLoad: function (instance, current) {
					current.$content.trigger('contentload');
				},
			});
		}, 3000);

	});

	$target.on('click', '.btn--count-plus', function() {
		var $this = $(this),
			$inp = $this.closest('.inp'),
			$input = $inp.find('.inp__text--count '),
			val = parseInt($input.val()),
			dataMax = $input.data('max') || $input.data('max') == 0 ? parseInt($input.data('max')) : 99999;

		if (dataMax == 0) {
			console.log('remove max');
			$.fancybox.open({
				src: '#popup-remove-max',
				type: 'inline',
			});
		} else if (val == dataMax) {
			console.log('remove alert');
			$.fancybox.open({
				src: '#popup-remove-alert',
				type: 'inline',
			});
		}
	});

	// allow cookieName write
	$target.on('click', '.js-allow', function () {
		var $this = $(this),
			cookieName = $this.data('cookie-name');

		document.cookie = cookieName+'=1;';
		$.fancybox.close();
	});

	// Fix fox Slick slides position (translate3d) inside Fancybox
	$target.find('[data-fancybox]').fancybox({
		afterShow: function() {
			var $this = $(this)[0]['$content'];

			$this.find('.slick-initialized').each(function() {
				$(this).slick('refresh');
			});
		},
	});

	// AJAX
	$target.on('click', '.js-fancybox-ajax', function (e) {
		e.preventDefault();
		var $this = $(this);

		$.fancybox.close(true);

		$.fancybox.open({
			src: $this.attr('href'),
			type: 'ajax',

			opts: {
				ajax: {
					settings: {
						dataType: 'html',
						dataFilter: function (data) {

							try {
								const resp = JSON.parse(data);

								if (resp.redirect) {
									window.location.replace(resp.redirect);
								} else {
									const fancySnippetName = $this.data('snippetname');
									// TODO REPLACE All SNIPPETs
									return '<div id="' + fancySnippetName + '">' + resp.snippets[fancySnippetName] + '</div>';
								}

							} catch (e) {
								if (e instanceof SyntaxError) {
									return data;
								} else {
									// console.log(e);
								}
							}
						},
					},
				},
				infobar: false,
				loop: false,
				arrows: false,
				toolbar: false,
				afterLoad: function (instance, current) {
					current.$content.trigger('contentload');
				},
			},
		});
	});

};
