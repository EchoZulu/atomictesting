import $ from 'jquery';

export const init = ($target) => {

	$target.find('.inp-custom-select').each(function() {
		var $this = $(this),
			$select = $this.find('.inp-custom-select__select'),
			$selectWrap = $this.find('.inp-custom-select__select-wrap'),
			$active = $this.find('.inp-item__inp:checked').closest('.inp-item').clone();

		const findActive = () => {
			$active.prop('checked', true);
			$selectWrap.html('<span class="inp-custom-select__item inp-item">' + $active.text() + '</span>');
			$this.removeClass('is-open');
		};

		findActive();


		// open
		$select
			.on('click', function(e) {
				e.preventDefault();
				if ( ! $( e.target ).closest('.inp-custom-select__wrapper').length ) {
					$this.toggleClass('is-open');
				}
			});

	});
};
