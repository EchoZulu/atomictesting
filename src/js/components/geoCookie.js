import Cookies from 'cookies-js';
import geolib from 'geolib';
import $ from 'jquery';

export const init = () => {

	const geolat = Cookies.get('geolat');
	const geolon = Cookies.get('geolon');

	const showLimit = 50; //km

	if (typeof geolat !== 'undefined' && typeof geolon !== 'undefined') return;

	navigator.geolocation.getCurrentPosition((position) => {
		const place = {
			longitude: 14.4042828,
			latitude: 50.0687858,
		};

		const { latitude, longitude } = position.coords;
		const distance = geolib.getDistance({latitude, longitude}, place, 1000) / 1000;
		const expires = 60 * 60 * 24; // 1 den v sekundách
		Cookies.set('geolat', latitude, { expires });
		Cookies.set('geolon', longitude, { expires });

		if (distance <= showLimit) {
			showNearestPlace();
		}

	});


	const showNearestPlace = () => {
		$('.js-nearest').show();
	};

};
