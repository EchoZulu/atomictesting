import $ from 'jquery';
import 'jquery-match-height';

export const init = ($target) => {

	var option = {
		byRow: true,
		property: 'height',
		target: null,
		remove: false,
	};

	$target.find('.b-product__properties-wrap').matchHeight(option);

	// Footer
	$target.find('.c-compare__helper, .c-compare__product').matchHeight(option);
	$target.find('.c-compare__param--title').matchHeight(option);

	// Compare
	$target.find('.c-compare__header .mh-compare').each(function (i, el) {
		var $that = $(el);

		var $sameItems = $('.c-compare .mh-compare').filter(function(i, b) {
			return $(b).index() == $that.index();
		});

		$sameItems.matchHeight({
			byRow: false,
			property: 'height',
			target: null,
			remove: false,
		});
	});

};
