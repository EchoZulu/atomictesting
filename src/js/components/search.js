import $ from 'jquery';

import '../class/sk';
import '../class/sk.widgets.Suggest';
import '../class/sk.widgets.SuggestMenu';
import { MQ } from '../tools/MQ';

export const init = ($target) => {

	$(window).resize(function() {
		var $input = $target.find('.f-search__text'),
			placeholder = $input.attr('placeholder'),
			dataPlaceholder = $input.data('placeholder'),
			dataPlaceholderMd = $input.data('placeholder-md');
		if ( MQ('mdUp') && MQ('lgDown') ) {
			if (placeholder != dataPlaceholderMd) {
				$input.attr('placeholder', dataPlaceholderMd);
			}
		} else {
			if (placeholder != dataPlaceholder) {
				$input.attr('placeholder', dataPlaceholder);
			}
		}
	});

	$('.f-search--suggest').each(function() {

		var $this = $(this),
			$input = $this.find('.f-search__text'),
			$wrap = $this.find('.f-search__suggest');

		if ( MQ('mdUp') && MQ('lgDown') ) {
			$input.attr('placeholder', $input.data('placeholder-md'));
		}

		var Suggest = new sk.widgets.Suggest($input, {
			minLength: 2,
			typeInterval: 100, // ms
			url: $this.attr('action'),
		});

		var $menu = $('<div class="b-suggest"></div>').appendTo( $wrap );

		var SuggestMenu = new sk.widgets.SuggestMenu($menu, Suggest, {
			'item': '.f-search__item',
		}).init();

		$(SuggestMenu)
			.on('menuselect', function(e) {
				var $item = this.$items.eq( this.selectedIndex-1 );

				if ( $item.hasClass('b-suggest__btn') ) {
					e.preventDefault();
					$this.submit();
					// return false;
				}
				else {
					window.location = $item.find('a').attr('href');
				}
			});

		$this.on('click', '.b-suggest__btn', function(e) {
			e.preventDefault();
			$this.submit();
		});

		return;
	});

};
