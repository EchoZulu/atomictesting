import $ from 'jquery';

export const init = ($target) => {

	$(document)
		.on('click', '.js-open-link', function(e) {
			e.preventDefault();

			var $this = $(this),
				$wrap = $this.closest('.js-open-wrap');

			$wrap.toggleClass('is-open');

		}).on('mouseenter', '.b-basket', function() {
			$target.find('.b-login').removeClass('is-open');
			$(this).addClass('is-hover');
		}).on('mouseleave', '.b-basket', function() {
			$(this).removeClass('is-hover');
		}).on('click', '.touchevents .js-touch-open__link', function(e) {
			var $this = $(this),
				$wrap = $this.closest('.js-touch-open');

			if (!$wrap.hasClass('is-open')) {
				e.preventDefault();
				$target.find('.js-touch-open').removeClass('is-open');
				$wrap.addClass('is-open');
			}

		});



};
