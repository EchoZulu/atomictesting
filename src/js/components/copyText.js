import $ from 'jquery';

export const init = () => {

	var $target = null;

	$(document)
		.on('click', '.js-copy-text', function(e) {
			e.preventDefault();

			const $this = $(e.currentTarget);
			const $inp = $this.closest('.inp');
			const $inpText = $inp.find('.inp__text');
			const value = $inpText.val();

			const $default = $this.find('.btn__text--default');
			const $success = $this.find('.btn__text--success');

			$target = value;
			document.execCommand('copy');

			$this
				.clearQueue()
				.css('pointer-events', 'none')
				.delay(1600)
				.queue(function (next) {
					$(this).css('pointer-events', 'auto');
					next();
				});
			$default
				.clearQueue()
				.fadeOut()
				.delay(900)
				.fadeIn();
			$success
				.clearQueue()
				.delay(300)
				.fadeIn()
				.delay(300)
				.fadeOut();
		});


	document.addEventListener('copy', function(e) {
		if ($target) {
			var text = $target;

			// replace tabů a odřádkování
			text = text.replace(/\s{2,}/g, ' ');
			text = text.replace(/\t/g, ' ');
			text = text.toString().trim().replace(/(\r\n|\n|\r)/g,'');

			if (e.clipboardData) {
				e.clipboardData.setData('text/plain', text);
			}
			else if (window.clipboardData) {
				window.clipboardData.setData('text', text);
			}

			$target = null;
			e.preventDefault();
		}
	});

};
