import $ from 'jquery';

export const init = () => {

	$(document)
		.on('click', function(e) {

			$('.js-etarget').each(function() {
				var $this = $(this);

				if ( ! $(e.target).closest($this).length ) {
					$this.removeClass('is-open');
				}
			});

		});

};
