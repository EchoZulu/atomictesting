import $ from 'jquery';
import '@fancyapps/fancybox';

export const init = () => {

	var buildResponse = function(response, $this, append) {
		if (response.snippets) {
			for (var i in response.snippets) {
				var $el = $('#' + i),
					$close = $el.find('.fancybox-close-small').clone();

				if ($el.is('[data-ajax-append]') && append) {
					$el.append(response.snippets[i]);
				}
				else {
					$el.html(response.snippets[i]);
				}

				// close fix
				if ( $close.length ) {
					$el.append($close);
				}

				$el.trigger('contentload');
			}
		}
		else if (response.redirect) {
			window.location.replace(response.redirect);
		}
		else {
			var $closef = $this.find('.fancybox-close-small').clone();
			$this.replaceWith( response );

			// close fix
			if ( $closef.length ) {
				$el.append($closef);
			}
		}
	};

	// set default
	const stateObj = {
		type: 'closedFancybox',
		loader: true,
		appendResponse: false,
		href: window.location.href,
		title: $('title').text(),
		index: 0,
		baseClass: false,
	};

	// replaceState
	history.replaceState(stateObj, null, window.location.href);
	console.log('now')

	// set lastState
	let lastState = null,
		undefined;

	// change types
	const changeState = (state) => {
		if ( !lastState && state.type == 'closedFancybox' ) {
			window.location.reload();
		}

		else if (state.type === 'closedFancybox') {
			if ( $('.fancybox-container').length ) {
				$.fancybox.close( true );
			}
			else {
				window.location.reload();
			}
		}

		else if ( state.type === 'openFancybox' ) {
			$.fancybox.close( true );

			var touch = {
				vertical : true,  // Allow to drag content vertically
				momentum : true,   // Continue movement after releasing mouse/touch when panning
			};

			if ( !Modernizr.touchevents ) {
				touch = false;
			}

			$.fancybox.open({
				src: state.href,
				type: 'ajax',
				opts: {
					infobar: false,
					loop: false,
					buttons: [],
					arrows: false,
					toolbar: false,
					clickSlide: false,
					clickOutside: false,
					touch: touch,
					baseClass: state.baseClass,
					btnTpl: {
						smallBtn: '<a href="#" class="fancybox-close-small circle circle--xl circle--highlight" data-fancybox-close><span class="icon-svg icon-svg--cross "><svg class="icon-svg__svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/static/img/bg/icons-svg.svg#icon-cross" x="0" y="0" width="100%" height="100%"></use></svg></span><span class="vhide">{{CLOSE}}</span></a>',
					},
					afterLoad: function(instance, current) {
						current.$content.trigger('contentload');
						current.$slide.on('click', function(e) {
							if ( $(e.target).hasClass('fancybox-slide') ) {
								$('[data-fancybox-close]').trigger('click.fb-close');
							}
						});
					},
					beforeClose: function(instance, current, target) {
						if ( target != undefined) {
							const state = {
								type: 'closedFancybox',
								loader: stateObj.loader,
								appendResponse: stateObj.loader,
								href: stateObj.href,
								title: stateObj.title,
								index: stateObj.index,
							};

							history.pushState(state, null, stateObj.href);
							changeState(state, false, state.appendResponse);
						}
					},
				},
			});
		}

		else if (state.type === 'ajaxChange') {
			$.ajax({
				url: state.href,
				beforeSend: function() {
					if ( state.loader ) {
						$('body').addClass('page-loading');
					}
				},
			})
			.done(function(response) {
				if ( state.loader ) {
					$('body').removeClass('page-loading');
				}
				buildResponse(response, false, state.appendResponse);
			});
		}

		// update title
		$('title').text(state.title);

		// set last instance
		lastState = state;
	};

	// pop states
	$(window)
		.on('popstate', function(e) {
			changeState(e.originalEvent.state);
		});

	// binds
	$(document)
		// history links
		.on('click', '.js-fancybox-open, .js-fancybox-form', function(e) {
			e.preventDefault();

			var $this = $(this);

			const state = {
				type: 'openFancybox',
				loader: true,
				appendResponse: false,
				href: $this.attr('href'),
				title: $this.data('title'),
				baseClass: $this.hasClass('js-fancybox-open') ? 'fancybox-page' : false,
			};

			history.pushState(state, null, $(this).attr('href'));
			changeState(state);
		})
		// ajax tabs
		.on('click', '.js-change-ajax', function(e) {
			e.preventDefault();

			var $this = $(this);

			const state = {
				type: 'ajaxChange',
				loader: $this.is('[data-loading-inline]') ? false : true,
				appendResponse: $this.is('[data-append-response]') ? true : false,
				href: $this.attr('href'),
				title: $this.data('title'),
				index: $this.parent().index(),
				baseClass: false,
			};

			history.pushState(state, null, $(this).attr('href'));
			changeState(state);
		})
		// ajax links - just change content after response
		.on('click', '.js-link', function(e) {
			e.preventDefault();

			var $this = $(this);

			$.ajax({
				url: $this.attr('href'),
				beforeSend: function() {
					if ( $this.find('.circle').length ) {
						$this.find('.circle').addClass('is-loading');
					}
					else {
						$this.addClass('is-loading');
					}
				},
			}).done(function(response) {
				if ( $this.find('.circle').length ) {
					$this.find('.circle').removeClass('is-loading');
				}
				else {
					$this.removeClass('is-loading');
				}

				buildResponse(response, $this);
			});
		})
		// form button submit
		.on('click', '.f-ajax button:submit', function(e) {
			e.preventDefault();

			var $that = $(this);

			if ( !$that.data('confirm') ) {
				$that.closest('.f-ajax').trigger('submit', [ $(this).attr('name') ]);
			}
		})
		// form submit
		.on('submit', '.f-ajax', function(e, btnName) {
			e.preventDefault();

			var $this = $(this);

			$this.find('.message').slideUp();
			$this.find('.has-error').removeClass('has-error');

			$.ajax({
				url: $this.attr('action'),
				method: $this.attr('method') ? $this.attr('method') : 'POST',
				data: btnName ? $this.serialize() + '&' + btnName + '=1' : $this.serialize(),
				beforeSend: function() {
					if ( !$this.hasClass('f-ajax--no-loader') ) {
						$this.addClass('is-loading');
					}
				},
			}).done(function(response) {
				if ( !$this.hasClass('f-ajax--no-loader') && !response.redirect ) {
					$this.removeClass('is-loading');
				}

				if ( !$this.hasClass('f-ajax--no-replace') ) {
					buildResponse(response, $this);
				}
			});
		})
		// trigger submit form after inp change
		.on('keyup change', '.js-ajax-inp', function() {
			var $form = $(this).closest('form');

			$.ajax({
				url: $form.attr('action'),
				method: $form.attr('method') ? $form.attr('method') : 'POST',
				data: $form.serialize(),
			});
		});

};
