import $ from 'jquery';
import throttle from 'lodash.throttle';
import { MQ } from '../tools/MQ';

export const init = ($target) => {

	$(document)
		.on('click', '.js-filter__link', function (e) {
			e.preventDefault();

			var $this = $(this),
				$group = $this.closest('.js-filter'),
				$content = $group.find('.js-filter__content');

			if ( $group.hasClass('is-open') ) {
				$content.slideUp(function() {
					$group.removeClass('is-open');
				});
			}
			else {
				$content.slideDown(function() {
					$group.addClass('is-open');
				});
			}
		})
		.on('click', '.js-toggle-filter__link', function (e) {
			e.preventDefault();

			var $this = $(this),
				$parent = $this.closest('.js-toggle-filter'),
				$content = $parent.find('.js-toggle-filter__content'),
				$select = $target.find('.f-sort__select');

			if ( $content.hasClass('is-open') ) {
				$content.slideUp(function() {
					$content.removeClass('is-open');
				});
				$this.removeClass('is-open');
			}
			else {
				$content.slideDown();
				$content.addClass('is-open');
				$this.addClass('is-open');
				$select.removeClass('is-open');

			}
		});

	$(window)
		.on('resize', throttle(function(e) {
			if ( MQ('lgUp') ) {
				var $content = $('.js-toggle-filter__content');

				$content.removeAttr('style');
			}

		}, 100, {
			trailing: true,
		}));

};
