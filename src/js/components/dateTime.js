import $ from 'jquery';
import 'kbw-datepick/dist/js/jquery.plugin.min.js';
import 'kbw-datepick/dist/js/jquery.datepick.min.js';
import 'kbw-datepick/dist/js/jquery.datepick-cs.js';
import 'kbw.timeentry';

export const init = ($target) => {
	const selectors = {
		DATEPICK: 'inp__datepick',
		DATETIME: 'inp__datetime',
		FORM: 'f-product-borrow',
	};

	// time
	$target.find('.'+selectors.DATETIME).each(function() {
		var $this = $(this);

		$.timeEntry.setDefaults({spinnerImage: ''});
		$this.timeEntry({
			timeSteps: [1, 60, 0],
			datetimeFormat: 'H:M',
		});

		$this.on('change', function(e) {
			var $form = $(this).closest('form'),
				$btn = $form.find('button');

			$btn.addClass('is-disabled');

			setTimeout(() => {
				$form.submit();
				$btn.removeClass('is-disabled');
			}, 2000);
		});
	});

	// date
	$target.find('.'+selectors.DATEPICK).each(function() {
		var $this = $(this),
			$from = $('#startpicker'),
			$to = $('#endpicker');

		$this.datepick({
			dateFormat: 'dd. mm. yyyy',
			minDate: '+1d',
			// maxDate: '+2m',
			showAnim: '',
			onSelect: customRange,
			onDate: disableDates,
			showTrigger: '.icon-svg--calendar',
		});

		var disabledDates = $from.data('disabled-dates');

		// if ( $from.val() ) {
		// 	var date = $from.val().split('. '),
		// 		newDate = null;

		// 	for (var i = 0; i < disabledDates.length; i++) {
		// 		if ( (date[1] <= disabledDates[i][0]) && (date[0] <= disabledDates[i][1]) && (date[2] <= disabledDates[i][1]) ) {
		// 			newDate = new Date(disabledDates[i][2], disabledDates[i][0] - 1, disabledDates[i][1]);
		// 			break;
		// 		}
		// 	}

		// 	$to.datepick('option', 'minDate', new Date(date[2], date[1] - 1, date[0]) || null);
		// 	$to.datepick('option', 'maxDate', newDate || null);
		// }

		function disableDates(date, inMonth) {
			if (inMonth) {
				for (var i = 0; i < disabledDates.length; i++) {
					if (date.getMonth() + 1 == disabledDates[i][0] && date.getDate() == disabledDates[i][1] &&  date.getFullYear() == disabledDates[i][2] ) {
						return {dateClass: 'off_day', selectable: false};
					}
				}
			}
			return {};
		}

		function customRange(date) {
			var $this = $(this),
				$parent = $this.closest('.'+selectors.FORM);

			if (this.id == 'startpicker') {
				$to.datepick('option', 'minDate', date[0] || null);
				// $to.datepick('option', 'maxDate', null);

				// for (var i = 0; i < disabledDates.length; i++) {
				// 	if ( (date[0].getMonth() + 1 <= disabledDates[i][0]) && (date[0].getDate() <= disabledDates[i][1]) && (date[0].getFullYear() <= disabledDates[i][2]) ) {
				// 		$to.datepick('option', 'maxDate', new Date(disabledDates[i][2], disabledDates[i][0] - 1, disabledDates[i][1]) || null); // date[0]
				// 		return false;
				// 	}
				// }
			}

			if ( $from.val() > $to.val() ) {
				$to.attr('value', '');
			}

			if ( $from.val() && $to.val() ) {
				$parent.submit();
			}
		}

	});

};
