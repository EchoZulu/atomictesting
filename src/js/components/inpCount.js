import $ from 'jquery';


export const init = () => {
	const $document = $(document);

	const inBetween = (value, range) => {
		const [min, max] = range;

		return (min === false || value >= min) && (max === false || value <= max);
	};

	const getRange = ($input) => {
		const min = $input.attr('min');
		const max = $input.attr('max');

		return [
			typeof min !== 'undefined' ? parseInt(min) : false,
			typeof max !== 'undefined' ? parseInt(max) : false,
		];
	};

	const addValue = ($input, value) => {
		const currentValue = parseInt($input.val());
		const newValue = currentValue + value;
		const range = getRange($input);

		if (range && !inBetween(newValue, range)) return;

		$input.val(newValue).trigger('change');

	};

	const handleClick = (event) => {
		event.preventDefault();

		const $target = $(event.currentTarget);
		const $input = $target.closest('.inp').find('.inp__text--count');
		const value = $target.data('add');

		addValue($input, value);
	};

	$document.on('click', '[data-add]', handleClick);
};
