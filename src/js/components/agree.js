import $ from 'jquery';
import { RefCountDisposable } from 'rx';


export const init = ($target) => {
	const log = console.log;

	$(document)
		.on('click', '.js-agree', function(e) {
			var $popup = $target.find('#popup-remove'),
				$confirm = $popup.find('.js-agree__confirm'),
				removeUrl = $(e.currentTarget).attr('href');

			if ($popup.length && $confirm.length) {
				e.preventDefault();


				if (removeUrl) {
					$confirm.attr('href', removeUrl);
					//send detalayer
					const data = $(this).data('layer');

					if (data) {
						$confirm.attr('data-layer', JSON.stringify(data));
					}
				}

				$.fancybox.open({
					src: $popup,
					type: 'inline',
					afterLoad: function (instance, current) {
						current.$content.trigger('contentload');
					},
				});
			}
		})
		.on('click', '.js-agree__confirm', function() {
			$target.find('.js-agree__form').submit();

			var $popup = $target.find('#popup-remove');
			$.fancybox.close({
				src: $popup,
			});
		});
};
