import $ from 'jquery';
import detectIt from 'detect-it';

export const init = ($target) => {
	// touchevents
	if (detectIt.primaryInput === 'mouse') {
		$('html').addClass('no-touchevents');
	}
	else {
		$('html').addClass('touchevents');
	}


	// objectfit
	if ( $('html').hasClass('no-objectfit') ) {
		$target.find('.img--fill').each(function () {
			var $container = $(this).find('.img__holder'),
				imgUrl = $container.find('img').prop('src'),
				imgDataUrl = $container.find('img').data('src');

			if (imgUrl) {
				$container.css('backgroundImage', 'url(' + imgUrl + ')');

			} else if (imgDataUrl) { // lozy load objectfit fallback
				$container.data('background-image', imgDataUrl);
				$container.addClass('lozad');
			}
		});
	}
};
