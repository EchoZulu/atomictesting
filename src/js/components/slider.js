import $ from 'jquery';
import wNumb from 'wnumb/';
import noUiSlider from 'nouislider/';

export const init = ($target) => {

	$target.find('.js-slider').each(function() {
		var $this = $(this),
			$inpFrom = $this.find('.js-slider__inp--from'),
			$inpTo = $this.find('.js-slider__inp--to'),
			$inpFrom2 = $this.find('.js-slider__inp--from2'),
			$inpTo2 = $this.find('.js-slider__inp--to2'),
			$slider = $this.find('.js-slider__range').get(0),
			start = Number($this.data('start')),
			end = Number($this.data('end')),
			min = Number($this.data('min')),
			max = Number($this.data('max')),
			step = Number($this.data('step')),
			tooltips = $this.data('tooltips'),
			slider = null;

		slider = noUiSlider
			.create($slider, {
				start: [ start, end ],
				connect: true,
				step: step,
				tooltips: tooltips,
				range: {
					'min': min,
					'max': max,
				},
			});


		let decimals = 0
        if (step < 1) {
            decimals = 1;
        }

		var moneyFormat = wNumb({
			decimals: decimals,
			thousand: ' ',
			suffix: ' ' + $this.data('unit'),
		});

        let parse;
        if (step < 1) {
            parse = parseFloat;
        } else {
            parse = parseInt;
        }

		// init
		$inpFrom.val(moneyFormat.to($slider.noUiSlider.get()[0]*1));
		$inpTo.val(moneyFormat.to($slider.noUiSlider.get()[1]*1));

		$inpFrom2.val(parse($slider.noUiSlider.get()[0]*1));
		$inpTo2.val(parse($slider.noUiSlider.get()[1]*1));



		// slider release -> send query to server
		$slider.noUiSlider.on('set', function(values) {
			$inpFrom2.val(parse(values[0]));
			$inpTo2.val(parse(values[1])).trigger('change');
		});

		// slide
		$slider.noUiSlider.on('slide', function(values) {
			$inpFrom.val(moneyFormat.to(parse(values[0])));
			$inpTo.val(moneyFormat.to(parse(values[1])));
		});


		// input write fix
		$inpFrom.change( function () {
			var $this = $(this);
			const val = $this.val().replace(',','.').replace(/[^0-9\.]/g,'');
			$inpFrom2.val(parse(val));
		});

        // input write fix
		$inpTo.change( function () {
			var $this = $(this);
            const val = $this.val().replace(',','.').replace(/[^0-9\.]/g,'');
            $inpTo2.val(parse(val));
		});

	});

};
