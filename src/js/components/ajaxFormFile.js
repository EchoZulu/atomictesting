import $ from 'jquery';
import '../class/jquery.form.min';

export const init = ($target) => {
	$target.find('.js-antispam .inp__text').each(function() {
		// antispam for form
		$(this).attr('value', $(this).closest('.js-antispam').find('label strong').text());
	});

	var buildResponse = function(response, $this) {
		if (response.snippets) {
			for (var i in response.snippets) {
				var $el = $('#' + i);
				if ($el.is('[data-ajax-append]')) {
					$el.append(response.snippets[i]);
				}
				else {
					$el.html(response.snippets[i]);
				}
				$el.trigger('contentload');
			}
		}
		else if (response.redirect) {
			window.location.replace(response.redirect);
		}
		else {
			$this.replaceWith( response );
		}
	};

	// file form
	$target.find('.f-ajax-file').each(function() {
		var $this = $(this);

		$this.off('submit');

		$this.on('submit', function(e) {
			e.preventDefault();

			$this.ajaxSubmit({
				url: $this.attr('action'),
				method: 'POST',
				data: $this.serialize(),
				beforeSubmit: function() {
					if ( !$this.hasClass('f-ajax--no-loader') ) {
						$this.addClass('is-loading');
					}
				},
				success: function(response) {
					// console.log(response)
					if ( !$this.hasClass('f-ajax--no-loader') ) {
						$this.removeClass('is-loading');
					}

					if ( !$this.hasClass('f-ajax--no-replace') ) {
						buildResponse(response, $this);
					}
				},
			});
		});

	});

};
