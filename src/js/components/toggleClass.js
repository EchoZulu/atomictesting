import $ from 'jquery';


export const init = ($target) => {
	const $document = $(document);
	const selectors = {
		TRIGGER: '[data-toggle-class]',
		CONTENT: '[data-toggle-class-content]',
		TIME: '[data-time-toggle]',
		TIMECLASS: '[data-time-toggle-class]',
	};

	const handleClick = (event) => {
		event.preventDefault();

		const $trigger = $(event.currentTarget);
		const $content = $trigger.closest(selectors.CONTENT);
		const className = $trigger.data('toggle-class');

		// show once responsive-hidden items
		if ( $trigger.closest('.b-gallery--hidable').length > 0 ) {
			$trigger.closest('.b-gallery--hidable').find('.b-gallery__item').removeClass('u-hide--md-down');
		}

		$trigger.toggleClass(className);
		$content.not($trigger).toggleClass(className);

		$( $trigger.data('toggle-class-content') ).toggleClass( $trigger.data('toggle-class') );

		if ( $trigger.data('toggle-others') == true ) {
			$( '.'+$trigger.data('toggle-class-content') ).not($trigger).removeClass(className);
		}
	};

	const handleChange = (event) => {
		event.preventDefault();

		const $trigger = $(event.currentTarget);
		const $content = $trigger.closest(selectors.CONTENT);
		const className = $trigger.data('toggle-class');
		const index = $trigger.find('option:checked').index();

		if ( $trigger.data('toggle-others') == true ) {
			$( '.'+$trigger.data('toggle-class-content')).removeClass(className);
		}


		$($( '.'+$trigger.data('toggle-class-content') )[index]).toggleClass( $trigger.data('toggle-class') );

	};

	$target.find('[data-time-toggle-class]').each(function() {
		var $this = $(this),
			timeClass = $this.data('time-toggle-class'),
			time = $this.data('time-toggle') ? $this.data('time-toggle') : 3000; // default 3s

		$this.removeClass(timeClass);

		setTimeout(function() {
			$this.addClass(timeClass);
		}, time);
	});

	$document
		.on('click', selectors.TRIGGER+':not(select)', handleClick) // not(select)
		.on('change', selectors.TRIGGER, handleChange);
};
