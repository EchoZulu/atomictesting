import $ from 'jquery';

export const init = () => {

	$(document)
		.on('click', '.js-close', function(e) {
			e.preventDefault();

			var $this = $(this),
				selector = $this.data('close-selector'),
				$parent = $this.closest(selector),
				cookieName = $this.data('cookie-name') ? $this.data('cookie-name') : 'visitor',
				$main = $('.main');

			$parent.slideUp();

			if (cookieName == 'firstOrderDiscount') {
				var now = new Date();
				now.setTime(now.getTime() + 9999*3600*1000);
				document.cookie = cookieName+'=close;expires='+now.toUTCString()+';path=/';
				setTimeout(function() {
					$main.removeClass('main--alert');
				}, 300);

			} else {
				document.cookie = cookieName+'=close';
			}

			$('.js-fixed--w-alert').removeClass('js-fixed--w-alert');
		});
};
