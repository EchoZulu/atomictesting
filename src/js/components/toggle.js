/* global Modernizr */
import $ from 'jquery';


export const init = () => {
	const $document = $(document);
	const selectors = {
		TRIGGER: '[data-toggle]',
		CONTENT: '[data-toggle-content]',
		HOVER: '[data-toggle-hover]',
	};
	const classes = {
		IS_OPENED: 'is-open',
	};

	const handleOpen = (event) => {
		const toggle = getToggle(event);
		const hasTouch = Modernizr.touchevents;
		const isMouseEnter = event.type === 'mouseenter';

		if ((toggle.isContent) || (hasTouch && isMouseEnter)) return;

		closeAll();

		if (toggle.isTrigger) {
			event.preventDefault();

			openIfClosed(toggle);
		}
	};

	const handleMouseLeave = () => {
		closeAll();
	};

	const getContentElement = ($trigger) => {
		const id = $trigger.data('toggle');

		return $(`[data-toggle-content="${id}"]`);
	};

	const getToggle = (event) => {
		const $this = $(event.target);
		const $hover = $this.closest(selectors.HOVER);
		const $trigger = $hover.length ? $hover.find(selectors.TRIGGER) : $this.closest(selectors.TRIGGER);
		const $content = getContentElement($trigger);
		const isTrigger = $trigger.length;
		const isContent = $this.closest(selectors.CONTENT).length;
		const isOpened = $trigger.hasClass(classes.IS_OPENED);

		return {
			$trigger,
			$content,
			isTrigger,
			isContent,
			isOpened,
		};
	};

	const closeAll = () => {
		const $triggers = $(selectors.TRIGGER);
		const $contents = $(selectors.CONTENT);

		$triggers.removeClass(classes.IS_OPENED);
		$contents.removeClass(classes.IS_OPENED);
	};

	const openIfClosed = (toggle) => {
		if (!toggle.isOpened) {
			toggle.$trigger.addClass(classes.IS_OPENED);
			toggle.$content.addClass(classes.IS_OPENED);
		}
	};

	const handleHeadroomUnpin = () => {
		const $header = $('.header');
		const $toggles = $header.find('[data-toggle], [data-toggle-content]');

		$toggles.removeClass('is-open');
	};


	$document
		.on('click', handleOpen)
		.on('mouseenter', selectors.HOVER, handleOpen)
		.on('mouseleave', selectors.HOVER, handleMouseLeave)
		.on('headroomUnpin', handleHeadroomUnpin);
};
