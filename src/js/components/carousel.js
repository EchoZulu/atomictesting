import $ from 'jquery';
import 'slick-carousel/slick/slick';
import { MQ } from '../tools/MQ';
import 'appear/dist/appear.js';

export const init = ($target) => {

	// Product Main Gallery
	$target.find('.b-product-gallery__list').not('.slick-initialized').each(function() {
		var $this = $(this),
			$parent = $this.closest('.b-product-gallery'),
			$prev = $parent.find('.b-product-gallery__prev'),
			$next = $parent.find('.b-product-gallery__next'),
			$galleryItems = $parent.find('.b-product-gallery__thumblist .b-product-gallery__thumbitem--active'),
			$popupScroll = $($target.find('.b-preview__list')[0]),
			$popupGalleryItems = $target.find('.b-tabs__fragment#foto').find('.b-preview__thumbitem');

		$this
			.slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				variableWidth: true,
				dots: false,
				infinite: false,
				responsive: [
					{
						breakpoint: 750,
						settings: {
							arrows: true,
							dots: true,
							prevArrow: $prev,
							nextArrow: $next,
						},
					},
				],
			})
			.on('beforeChange', function(e, slick, currentSlide, nextSlide) {
				$galleryItems.filter('.is-active').removeClass('is-active');
				$($galleryItems[nextSlide]).addClass('is-active');

				$popupScroll.slick('slickGoTo', nextSlide );
				$popupGalleryItems.filter('.is-active').removeClass('is-active');
				$($popupGalleryItems[nextSlide]).addClass('is-active');

			});

		$galleryItems
			.on('click', function(e) {
				e.preventDefault();
				$this.slick('slickGoTo', $(this).index());
				$galleryItems.filter('.is-active').removeClass('is-active');
				$(this).addClass('is-active');
			});
	});

	// References
	appear({
		elements: function elements() {
			return $target.find('.b-references__list').not('.slick-initialized');
		},
		appear: function appear(el) {
			$(el).each(function() {
				var $this = $(this),
					$parent = $this.closest('.b-references'),
					$scroll = $parent.find('.b-references__list'),
					$prev = $parent.find('.b-references__prev'),
					$next = $parent.find('.b-references__next'),
					$menuItems = $parent.find('.m-references .m-references__item'),
					speed = 300,
					time = 3000;

				$scroll
					.slick({
						slidesToShow: 1,
						slidesToScroll: 1,
						autoplay: true,
						autoplaySpeed: time,
						speed: speed,
						prevArrow: $prev,
						nextArrow: $next,
						infinite: true,
					})
					.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
						$menuItems
							.removeClass('is-active')
							.eq( nextSlide ).addClass('is-active');

					});

				$menuItems
					.on('click', function(e) {
						e.preventDefault();
						$scroll.slick('slickGoTo', $(this).index() );
					});
			});
		},
		bounds: 200,
	});

	// Attached Gallery (product, images, videos, articles, ...)
	appear({
		elements: function elements() {
			return $target.find('.b-gallery__carousel');
		},
		appear: function appear(el) {
			$(el).each(function() {
				var $this = $(this),
					$parent = $this.closest('.b-gallery'),
					$scroll = $this.find('.b-gallery__list'),
					$prev = $parent.find('.b-gallery__prev'),
					$next = $parent.find('.b-gallery__next'),
					$pager = $this.find('.b-gallery__pager'),
					$thumbs = $this.find('.b-gallery__thumbs'),
					$thumbsItem = $thumbs.find('.b-gallery__thumbs-item'),
					timeout = $this.data('timeout') ? $this.data('timeout') : false,
					infinite = $this.data('infinite') ? $this.data('infinite') : false,
					fade = $this.data('fade') ? true : false,
					adaptiveHeight = $this.data('adaptive-height') ? true : false,
					desktopSlides = $scroll.data('desktop-slides') ? $scroll.data('desktop-slides') : 3,
					speed = 300;

				if ( MQ('mdDown') ) {
					timeout = false;
				}

				$scroll
					.slick({
						slidesToShow: desktopSlides,
						slidesToScroll: 1,
						autoplay: timeout ? true : false,
						autoplaySpeed: timeout,
						infinite: timeout ? true : infinite,
						speed: speed,
						dots: $pager.length ? true : false,
						// appendDots: $pager,
						prevArrow: $prev,
						nextArrow: $next,
						adaptiveHeight: adaptiveHeight,
						fade: fade,
						waitForAnimate: false,
						responsive: [
							{
								breakpoint: 750,
								settings: {
									slidesToShow: 1,
									slidesToScroll: 1,
								},
							},
							{
								breakpoint: 1000,
								settings: {
									slidesToShow: 2,
									slidesToScroll: 1,
								},
							},
							{
								breakpoint: 1360,
								settings: {
									slidesToShow: 3,
									slidesToScroll: 1,
								},
							},
							{
								breakpoint: 1445,
								settings: {
									slidesToShow: 4,
									slidesToScroll: 1,
								},
							},
						],
					});

				$thumbsItem
					.on('click', function(e) {
						e.preventDefault();

						$scroll.slick('slickGoTo', $(this).index());
					});

			});
		},
		bounds: 200,
	});

	// Modal Product Gallery
	$target.find('.b-preview__list').not('.slick-initialized').each(function() {
		var $this = $(this),
			$parentMaster = $this.closest('.b-preview'),
			$parent = $this.closest('.b-preview__wrap'),
			$items = $this.find('.b-preview__item'),
			$prev = $parent.find('.b-preview__prev'),
			$next = $parent.find('.b-preview__next'),
			$more = $parentMaster.find('.b-preview__more'),
			$nexts = $.merge($next, $more),
			thumbs = $this.data('thumbs'),
			$galleryList = $parentMaster.find(thumbs),
			$galleryItems = $parentMaster.find(thumbs).find('.b-preview__thumbitem'),
			$mainScroll = $(document).find('.b-product-gallery__list'),
			$mainGalleryItems = $(document).find('.b-product-gallery__thumblink'),
			goto = $parent.data('goto') ? $parent.data('goto') : 0,
			$downloadLink = $parentMaster.find('.b-preview__download');

		$this
			.slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				prevArrow: $prev,
				nextArrow: $nexts,
				infinite: true,
				variableWidth: true,
				accessibility: true,
				focusOnChange: true,
			})
			.on('beforeChange', function(e, slick, currentSlide, nextSlide) {
				$galleryItems.filter('.is-active').removeClass('is-active');
				$($galleryItems[nextSlide]).addClass('is-active');

				// aktualizace download linku
				if ( $downloadLink.length > 0 ) {
					$downloadLink.attr('href', $($items[nextSlide]).find('img').attr('src') );
				}

				// synchronizace procházení s galerií na detailu produktu
				if ( $(e.currentTarget).closest('.b-preview__wrap').hasClass('b-preview__wrap--photo') ) {
					$mainScroll.slick('slickGoTo', nextSlide );
					$mainGalleryItems.filter('.is-active').removeClass('is-active');
					$($mainGalleryItems[nextSlide]).addClass('is-active');
				}

				// move items inside galleryList to be visible
				if ($($galleryItems[nextSlide]).overflown($galleryList)['width'] > 0) {
					$galleryList.animate({
						scrollLeft: $($galleryItems[nextSlide]).overflown($galleryList)['scrollLeft'],
					}, 300);

				} else if ($($galleryItems[nextSlide]).overflown($galleryList)['widthOpposite'] < 0) {
					$galleryList.animate({
						scrollLeft: $($galleryItems[nextSlide]).overflown($galleryList)['scrollLeftOpposite'],
					}, 300);
				} else if ($($galleryItems[nextSlide]).overflown($galleryList)['height'] > 0) {
					$galleryList.animate({
						scrollTop: $($galleryItems[nextSlide]).overflown($galleryList)['scrollTop'],
					}, 300);
				} else if ($($galleryItems[nextSlide]).overflown($galleryList)['heightOpposite'] < 0) {
					// console.log( $($galleryItems[nextSlide]).overflown($galleryList)['heightOpposite'] );
					$galleryList.animate({
						scrollTop: $($galleryItems[nextSlide]).overflown($galleryList)['scrollTopOpposite'],
					}, 300);
				}

			})
			.on('afterChange', function(e, slick, currentSlide) {
				// stop video
				$(e.currentTarget).find('.slick-slide:not(.slick-current)').find('iframe').each(function() {
					var video = $(this).attr('src');
					$(this).attr('src','');
					$(this).attr('src',video);

					// $(this)[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '}', '*');
				});
			}).on('init', function(e, slick) {
				$this.slick('slickGoTo', goto);
			});

		$this.trigger('init');
		$more.removeAttr('style'); // prevent displaying more arrow


		// side thumb navigation
		$galleryItems
			.on('click', function(e) {
				e.preventDefault();
				$this.slick('slickGoTo', $(this).index());
			});

	});


	$.fn.overflown=function(container) {
		var e=this[0];

		if(e) {
			var pos = {
				width: e.scrollWidth + e.offsetLeft - container.width() - container.scrollLeft() + 18,
				widthOpposite: e.offsetLeft - container.scrollLeft() + 18,
				scrollLeft: e.scrollWidth + e.offsetLeft - container.width() + 18,
				scrollLeftOpposite: e.offsetLeft,

				height: e.scrollHeight + e.offsetTop - container.height() - container.scrollTop(),
				scrollTop: e.scrollHeight + e.offsetTop - container.height(),
				heightOpposite: e.offsetTop - container.scrollTop(),
				scrollTopOpposite: e.offsetTop,
			};

			return pos;
		} else {
			return false;
		}

	};

	const handleToggleGallery = (e) => {
		var $this = $(e.currentTarget),
			$parent = $this.closest('.b-preview'),
			galleryWrap = $this.data('toggle-gallery');


		$parent.find('.b-preview__wrap').removeClass('is-active');
		$parent.find('.b-preview__wrap').filter(galleryWrap).addClass('is-active');

		// refresh for focusOnChange
		$parent.find('.b-preview__wrap.is-active').find('.slick-initialized').each(function() {
			$(this).slick('refresh');
		});

	};

	const handleChangeActiveItem = (e) => {
		var $this = $(e.currentTarget),
			activeTab = $this.data('active-item').split(',')[0],
			activeEl = $this.data('active-item').split(',')[1],
			$popupGallery = $(document).find('#popup-gallery'),
			$activeFragment = $popupGallery.find('.js-tabs__fragment.is-active').attr('id'),
			$popupScroll = $popupGallery.find('.b-preview__wrap--'+activeTab).find('.b-preview__list');

		// go to tab
		if ( activeTab != $activeFragment) {
			$popupGallery.find('[data-toggle-gallery=".b-preview__wrap--'+activeTab+'"]').click();
		}

		// go to slick slide
		$popupScroll.slick('slickGoTo', activeEl);

	};

	$(document)
		.on('click', '[data-toggle-gallery]', handleToggleGallery)
		.on('click', '[data-active-item]', handleChangeActiveItem);

};
