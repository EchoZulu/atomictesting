import $ from 'jquery';

export const init = () => {

	$(document)
		.on('click', '.js-remove-address__btn', function(e) {
			e.preventDefault();

			var $this = $(this),
				$parent = $this.closest('.js-remove-address'),
				$inpName = $parent.find('input[data-validate="name"]');

			$parent.slideUp();
			$inpName.val('');

		});

};
