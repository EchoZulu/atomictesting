import $ from 'jquery';

export const init = ($target) => {

	$target.find('.b-optional__price').each(function() {
		var $this = $(this),
			url = $this.data('mininstallment'),
			$parent = $this.closest('.b-optional__left');

		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'xml',
			success: function(xml) {
				var price = $(xml).find('vysledek').find('vyseSplatky').text();
				$this.text(price);
				$parent.removeClass('is-hidden');
			},
		});
	});

	const handleServicesChange = () => {
		var price = $target.find('.b-optional__link').data('price'),
			$warranties = $target.find('.b-warranty__inp:checked'),
			warranty = 0,
			newPrice = 0;

		$warranties.each(function() {
			warranty += parseInt( $(this).data('price') );
		});
		newPrice = parseInt(price) + parseInt(warranty);

		$target.find('.b-optional--installment .b-optional__link').each(function() {
			$(this).attr('href', $(this).data('url')+newPrice );
		});

		var url = 'https://www.cetelem.cz/webkalkulator.php?kodProdejce=2182699&kodBaremu=102&kodPojisteni=T1&cenaZbozi='+newPrice+'&primaPlatba=0&pocetSplatek=48';

		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'xml',
			success: function(xml) {
				var price = $(xml).find('vysledek').find('vyseSplatky').text();
				$target.find('.b-optional--installment .b-optional__price').text(price);
			},
		});
	};

	$(document)
		.on('change', '.b-warranty__inp', handleServicesChange);
};
