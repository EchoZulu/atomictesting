import $ from 'jquery';
import { EEXIST } from 'constants';

export const init = ($target) => {
	const $document = $(document);
	const selectors = {
		PARENT: 'f-open',
		PARENT_PERSONAL: 'f-open--personal',
		INPUT: 'f-open__inp',
		LINK: 'f-open__link',
		BOX: 'f-open__box',
		SELECTED_ADDRESS: 'f-items__item.is-checked',
		CLOSE_PERSONAL: '[data-close-personal]',
	};
	const classes = {
		IS_CHECKED: 'is-checked',
	};

	const handleChange = (event) => {
		const $toggle = $(event.target);
		const $parent = $toggle.closest('.'+selectors.PARENT);
		const $box = $($parent.find('.'+selectors.BOX)[0]);
		const animation = $parent.data('animation');

		if ($toggle.is(':checked')) {
			handleOpen($box, animation);
		} else {
			handleClose($box, animation);
		}
	};

	const handleToggle = (event) => {
		event.preventDefault();
		const $link = $(event.target);
		const $parent = $link.closest('.'+selectors.PARENT);
		const $box = $($parent.find('.'+selectors.BOX)[0]);
		const animation = $parent.data('animation');

		if ($box.is(':visible')) {
			handleClose($box, animation);
		} else {
			handleOpen($box, animation);
		}
	};

	const handleOpen = ($box, animation) => {
		if (animation == 'fade') {
			$box.fadeIn();
		} else {
			$box.slideDown();
		}
	};

	const handleClose = ($box, animation) => {
		if (animation == 'fade') {
			$box.fadeOut();
		} else {
			$box.slideUp();
		}
	};

	const handleClosePersonal = () => {
		const $personal = $target.find('.'+selectors.PARENT_PERSONAL);
		const $box = $($personal.find('.'+selectors.BOX)[0]);

		$personal.find('.'+selectors.INPUT).attr('checked', false);
		handleClose($box);
	};

	$document
		.on('change', '.'+selectors.INPUT, handleChange)
		.on('click', '.'+selectors.LINK, handleToggle)
		.on('click', selectors.CLOSE_PERSONAL, handleClosePersonal);

	// trigger
	$('.f-open__inp').trigger('change');

};
