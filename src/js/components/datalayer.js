import $ from 'jquery';
// import buildResponse from '../class/buildResponse';
const log = console.log;


export const init = () => {
	$(document)
        .on('change', 'input[data-layer]', function (e) {

	const newValue = $(this).val();
	const oldValue = $(this).data('original-amount');

	if (newValue != oldValue) {

	const data = $(this).data('layer');

	const newAmount = newValue - oldValue;
	if (newAmount >= 0) {
	dataLayer.push({
	'event': 'ec.addToCart',
	'ecommerce': {
	'currencyCode': 'CZK',
	'add': {               // 'add' actionFieldObject measures.
	'products': [{       //  adding a product to a shopping cart.
	'name': data.name,
	'id': data.id,
	'price': data.price,
	'brand': data.brand,
	'category': data.category,
	'variant': data.variant,
	'quantity': newAmount,
}],
},
},
});
} else {
	dataLayer.push({
	'event': 'ec.removeFromCart',
	'ecommerce': {
	'currencyCode': 'CZK',
	'remove': {               // 'add' actionFieldObject measures.
	'products': [{       //  adding a product to a shopping cart.
	'name': data.name,
	'id': data.id,
	'price': data.price,
	'brand': data.brand,
	'category': data.category,
	'variant': data.variant,
	'quantity': newAmount * -1,
}],
},
},
});
}
}
})
        .on('click', 'a[data-layer]', function (e) {

	log('click');
	if (!$(this).hasClass('js-agree')) {
	let remove = false;
	const data = $(this).data('layer');

                // log(data);

	let quality = 0;

                // log('ma property')
                // log(data.hasOwnProperty('quantity'))

	if (data.hasOwnProperty('quantity')) {
	quality = data.quantity;
} else if ($(this).data('add')) {
	quality = $(this).data('add');
}
                // log(quality);


	if (quality < 0) {
	quality = quality * -1;
	remove = true;
}


                // log(remove);
	if (!remove) {
	dataLayer.push({
	'event': 'ec.addToCart',
	'ecommerce': {
	'currencyCode': 'CZK',
	'add': {               // 'add' actionFieldObject measures.
	'products': [{       //  adding a product to a shopping cart.
	'name': data.name,
	'id': data.id,
	'price': data.price,
	'brand': data.brand,
	'category': data.category,
	'variant': data.variant,
	'quantity': quality,
}],
},
},
});
} else {
	dataLayer.push({
	'event': 'ec.removeFromCart',
	'ecommerce': {
	'currencyCode': 'CZK',
	'remove': {               // 'add' actionFieldObject measures.
	'products': [{       //  adding a product to a shopping cart.
	'name': data.name,
	'id': data.id,
	'price': data.price,
	'brand': data.brand,
	'category': data.category,
	'variant': data.variant,
	'quantity': quality,
}],
},
},
});
}


                // log(dataLayer);
}


});
};
