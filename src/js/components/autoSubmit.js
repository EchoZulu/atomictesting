import $ from 'jquery';

export const init = () => {

	$(document)
		.on('change', '.js-auto-submit', function(e) {
			e.preventDefault();

			$(this).closest('form').submit();
		});

};
