import $ from 'jquery';

export const init = () => {

	$(document)
		.on('click', '.js-element-clickable', function(e) {

			if ( $(e.target).closest('a, label, input, button', this).length ) return;

			e.preventDefault();

			if ($(this).find('.js-element-clickable__link').length ) {
				$(this).find('.js-element-clickable__link').get(0).click();
			}
			else {
				$(this).find('a').get(0).click();
			}
		});

};
