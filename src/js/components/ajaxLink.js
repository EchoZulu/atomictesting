import $ from 'jquery';
// import buildResponse from '../class/buildResponse';

export const init = () => {

	var buildResponse = function(response, $this) {
		if (response.snippets) {
			for (var i in response.snippets) {
				var $el = $('#' + i);

				if ($el.is('[data-ajax-append]')) {
					$el.append(response.snippets[i]);
				}
				else {
					$el.html(response.snippets[i]);
				}

				$el.trigger('contentload');
			}
		}
		else if (response.redirect) {
			window.location.replace(response.redirect);
		}
		else {
			$this.replaceWith( response );
		}
	};

	var request = null;

	$(document)
		.on('click', '.js-link-ajax', function(e) {
			e.preventDefault();
			var $this = $(this);
			$.ajax({
				url: $this.attr('href'),
				beforeSend: function() {
					$('body').addClass('page-loading');
				},
			})
			.done(function(response) {
				$('body').removeClass('page-loading');

				buildResponse(response, $this);

				var undefined;

				if (response.newUrl != undefined) {
					history.pushState({
						data: data,
						isFilter: true,
						formAction: formAction,
					}, '', response.newUrl);
				}
			});
		});

};
