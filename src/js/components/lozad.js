import lozad from 'lozad';

export const init = () => {

	const observer = lozad('.lozad', {
		rootMargin: '500px 0px',
		// threshold: 0.1,
	});
	observer.observe();
};
