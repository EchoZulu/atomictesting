import $ from 'jquery';

export const init = () => {

	$(document)
		.on('click', '.js-open-item', function (e) {
			e.preventDefault();

			var $this = $(this),
				$wrap = $this.closest('.js-open-wrap');

			$wrap.find('.b-career__wrap').slideToggle();
		});

}
