import $ from 'jquery';
import { MQ } from '../tools/MQ';

export const init = () => {

	$(window).on('scroll', function(e) {
		e.preventDefault();

		var $scroll = $(this).scrollTop();

		if ($scroll > 300 && MQ('mdUp')) {
			$('.top')
				.css('opacity', 1)
				.css('visibility', 'visible');
		} else {
			$('.top')
				.css('opacity', 0)
				.css('visibility', 'hidden');
		}
	});

};
