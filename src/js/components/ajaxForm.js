import $ from 'jquery';
import '@fancyapps/fancybox';

export const init = () => {
	var buildResponse = function(response, $this, ...currentTarget) {
		if (response.snippets) {

			for (var i in response.snippets) {
				var $el = $('#' + i),
					$close = $el.find('.fancybox-close-small').clone();

				if ($el.is('[data-ajax-append]')) {
					$el.append(response.snippets[i]);
				}
				else {
					$el.each(function() {
						$el.html(response.snippets[i]);
					});
				}

				// close fix
				if ( $close.length ) {
					$el.append($close);
				}

				if (i == 'snippet--prebasket') {
					$.fancybox.close(true);
					$.fancybox.open('<div id="snippet--prebasket">' + response.snippets[i] + '</div>');
				}
				else if (i == 'snippet--basketHeader' && $(currentTarget).hasClass('b-basket__form')) {
					$el.find('.b-basket').addClass('is-hover');
				}

				$el.trigger('contentload');
			}
		}

		else if (response.redirect) {
			window.location.replace(response.redirect);
		}
		else {
			var $holder = $this.parent();

			$this.replaceWith( response );

			$holder.find('> *').trigger('contentload');
		}
	};

	var request = null;


	var ajaxForm = function(e, $this) {

		if ($this.find('input[name="formSubmitAction"]').val()== 'continue2') {
			// basket
		} else {
			e.preventDefault();

			$this.find('.js-antispam .inp__text').each(function() {
				// antispam for form
				$(this).attr('value', $(this).closest('.js-antispam').find('label strong').text());
			});

			if (request != null) {
				request.abort();
			}

			request = $.ajax({
				url: $this.data('ajax-action') || $this.attr('action'),
				method: $this.attr('method'),
				data: $this.serialize(),
				beforeSend: function() {
					$this.addClass('is-loading');
				},
			})
			.done(function(response) {

				$this.removeClass('is-loading');

				request = null;
				buildResponse(response, $this, e.currentTarget);

			});
		}
	};



	$(document)

		//todo - toto odstranit
		.on('click', '.js-ajax-form button', function(e) {
			var $this = $(this);
			$this.before('<input type="hidden" name="' + $this.attr('name') + '" value="1">');
		})

		.on('click', '.js-ajax-form button', function(e) {
			var $this = $(this);
			$this.before('<input type="hidden" name="formSubmitAction" value="' + $this.attr('name') + '">');
		})


		.on('submit', '.js-ajax-form', function(e) {
			var $this = $(this);
			ajaxForm(e, $this);
		})
		.on('continueSubmit', '.js-validate', function(e) {
			var $this = $(this);
			ajaxForm(e, $this);
		});

};
