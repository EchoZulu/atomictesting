import $ from 'jquery';
import nanoScroller from 'nanoscroller';

export const init = () => {

	$('.nano').nanoScroller({ alwaysVisible: true });

};
