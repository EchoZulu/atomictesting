import $ from 'jquery';
import { MQ } from '../tools/MQ';

export const init = ($target) => {

	reverseTooltips();

	function reverseTooltips() {
		$target.find('.tooltip:not(.tooltip--static)').each(function() {
			var $this = $(this),
				pseudoAfterWidth = parseInt(window.getComputedStyle($this.get(0), ':after').width),
				left = $this.offset().left,
				windowWidth = $(window).width();

			if ( left + (pseudoAfterWidth) > windowWidth && MQ('smUp') ) { // default tooltip
				$this.addClass('tooltip--reversed');
			} else if ( (left + (pseudoAfterWidth / 2) ) > windowWidth) { // centered mobile tooltip
				$this.addClass('tooltip--reversed');
			} else {
				$this.removeClass('tooltip--reversed');
			}
		});

	}

	$(document).on('click', '.tooltip', function(e) {
		var $this = $(this),
			href = $(this).attr('href');

		if (href != '#') {
		} else {
			e.preventDefault();
			if ($this.hasClass('is-open')) {
				$this.removeClass('is-open');
			} else {
				$this.addClass('is-open');
			}
		}

	});

	$(window).on('resize', function() {
		reverseTooltips();
	});
};
