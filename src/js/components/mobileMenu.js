import $ from 'jquery';

export const init = () => {
	const $document = $(document);
	const $headerMenu = $('.m-mobile');
	const selectors = {
		TRIGGER: '[data-mobile-submenu-toggle]',
		BACK: '[data-mobile-submenu-back]',
		SUBMENU: '[data-mobile-submenu]',
	};

	const handleClick = (event) => {
		event.preventDefault();

		const $this = $(event.currentTarget);
		const $submenu = $this.next(selectors.SUBMENU);

		if (!$submenu.length) return;

		const submenuDepth = $submenu.attr('data-mobile-submenu');
		const submenuHtml = $submenu.html();
		const $wrap = $(`.m-mobile__wrap--depth-${submenuDepth} .nano-content`);

		const $nano = $(`.m-mobile__wrap--depth-${submenuDepth}.nano`);

		$wrap.html(submenuHtml);
		$headerMenu.attr('data-mobile-submenu-depth', submenuDepth);

		$nano.nanoScroller({ alwaysVisible: true });

		$(this).find('.icon-svg').each(function () {
			const svg = $(this).find('> svg').detach();
			setTimeout(() => {
				svg.appendTo(this);
			}, 0);
		});
	};

	const handleBack = (event) => {
		event.preventDefault();

		const currentDepth = $headerMenu.attr('data-mobile-submenu-depth');
		$headerMenu.attr('data-mobile-submenu-depth', currentDepth - 1);
	};


	$document
		.on('click', selectors.TRIGGER, handleClick)
		.on('click', selectors.BACK, handleBack);
};
