import $ from 'jquery';


export const init = () => {

	$(document)
		.on('click', '.inp-rating__stars label', function() {
			var $this = $(this),
				$parent = $this.closest('.inp-rating');

			$parent.addClass('is-visible');
		});
};
