import $ from 'jquery';
import validate from 'validate.js';
import defaultValidationConstraints from '../tools/defaultValidationConstraints';

export const init = ({ validationConstraints }) => {
	const _validationConstraints = validationConstraints ? validationConstraints : defaultValidationConstraints;
	const $document = $(document);
	// const $inputs = $('[data-validate][required]'); // validate all inputs (with not required but not empty)
	const $inputs = $('[data-validate]');

	const getFormInputs = ($form) => {
		const $blockOptional = $('.js-validate__optional').has('[data-validate-optional]:not(:checked)');
		const $inputsOptional = $blockOptional.find('[data-validate]');
		const $inputs = $form.find('[data-validate]').not($inputsOptional); // + neprazdne kterej nejsou required
		const $notRequiredEmpty = $form.find('[data-validate]').not('[required]').filter(function() {
			return !this.value;
		});

		// nepovinné položky, které jsou prázdné (není potřeba je validovat) - nepovinné neprázdné validovat
		return $inputs.not($notRequiredEmpty.not('[type="password"]'));
		// return $inputs;
	};

	const getConstraints = ($inputs) => {
		const constraints = {};

		$inputs.filter('[type="checkbox"]').map((index, item) => {
			const dataValidate = $(this).data('validate');
			const name = item.name;

			constraints[name] = _validationConstraints[dataValidate];
		});

		// stará validate podle name
		// $inputs.serializeArray().forEach((item) => {
		// 	const name = item.name;

		// 	if (_validationConstraints[name]) {
		// 		constraints[name] = _validationConstraints[name];
		// 	}
		// });

		$inputs.map(function() {
			const dataValidate = $(this).data('validate');
			const name = $(this).attr('name');

			if (_validationConstraints[dataValidate]) {
				constraints[name] = _validationConstraints[dataValidate];
			}
		}).get();

		return constraints;
	};

	const checkForErrors = ($form, $inputs) => {
		const constraints = getConstraints($inputs);
		const errors = validate($form[0], constraints) || {};

		return errors;
	};

	const handleFormSubmit = (event) => {
		const $form = $(event.target);

		event.preventDefault();

		if ($(event.currentTarget).data('scroll-top')) {
			$('html, body').animate({
				scrollTop: 0,
			}, 300);
		}

		const $inputs = getFormInputs($form);
		const errors = checkForErrors($form, $inputs);

		showErrors($inputs, errors || {});

		if ($.isEmptyObject(errors)) {
			if ( $form.hasClass('js-validate-ajax') ) {
				$form.trigger('continueSubmit');
			} else {
				$form[0].submit();
			}
		}
		return false;
	};

	const handleInputChange = (event) => {
		const $input = $(event.target);
		const name = $input.attr('name');
		// const dataValidate = $input.data('validate');
		const $form = $input.closest('.js-validate');

		if (!$form.length) return;

		const $inputs = getFormInputs($form);
		const errors = checkForErrors($form, $inputs);

		showErrorsForInput($input, errors[name]);
	};

	const showErrors = ($inputs, errors) => {
		$inputs.each((index, input) => {
			const $input = $(input);
			const name = $input.attr('name');

			showErrorsForInput($input, errors[name]);
		});
	};

	const showErrorsForInput = ($input, errors) => {
		const $formGroup = $input.closest('.js-validate__item');
		const $messages = $formGroup.find('.js-validate__message');

		resetFormGroup($formGroup);

		if (errors) {
			$formGroup.addClass('has-error');

			errors.forEach((error) => {
				addError($messages, error);
			});
		} else {
			$formGroup.addClass('has-success');
		}
	};

	const resetFormGroup = ($formGroup) => {
		const $messages = $formGroup.find('.help-block.error');

		$formGroup.removeClass('has-error has-success');
		$messages.remove();
	};

	const addError = ($messages, error) => {
		const $error = $(`
			<span class="help-block error">${error}</span>
		`);

		$error.appendTo($messages);
	};


	$document
		.on('submit', '.js-validate', handleFormSubmit)
		.on('change', $inputs, handleInputChange);
};
