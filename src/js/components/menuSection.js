import $ from 'jquery';
import { MQ } from '../tools/MQ';

export const init = ($target) => {

	var $fixed = $('.js-fixed'),
		$links = $('.js-fixed .b-tabs__link'),
		$tabs = $('.js-fixed .b-tabs__item'),
		headerHeight = 0;

	// Anchors corresponding to menu items
	var $items = $links.map(function() {
		var item = $( $(this).attr('href') );
		if ( item.length ) {
			return item;
		}
	});

	$(window).on('scroll', function() {
		headerHeight = $target.find('.headroom--pinned') ? $('.headroom--pinned').outerHeight() : 0;

		// Get container scroll position
		var fromTop = $(this).scrollTop();

		// Get id of current scroll item
		var cur = $items.map(function() {
			if ( $(this).offset().top + $(this).outerHeight() - headerHeight - $('.js-tabs').outerHeight() >= fromTop ) {
				return this;
			}
		});

		// Get the id of the current element
		// cur = cur[cur.length-1];
		var id = cur && cur.length ? cur[0].attr('id') : '';

		// Set/remove active class
		$tabs
			.removeClass('is-active')
			.find('.b-tabs__link')
			.filter('[href="#' + id + '"]')
			.closest('.b-tabs__item')
			.addClass('is-active');


		clearTimeout($.data(this, 'scrollTimer'));
		$.data(this, 'scrollTimer', setTimeout(function() {
			var $item = $fixed.find('.js-tabs__item.is-active'),
				$underline = $fixed.find('.b-tabs__underline');

			if ( $underline.length > 0  ) {
				underlineArrange($item, $underline);
			}
		}, 100));
	});

	function underlineArrange($item, $underline) {
		if ( MQ('lgUp') ) {
			$underline.animate({
				'left': $item.position().left  + parseInt($item.css('marginLeft')) - (parseInt($item.find('.js-tabs__link').css('paddingLeft')) / 2),
				'width': $item.innerWidth(),
			}, 300);

			$underline.css({
				'display': 'block',
			});

		} else if ( MQ('lgDown') ) {
			$underline.css({
				'display': 'none',
			});
		}
	}

	$(window).trigger('scroll');
};
