import $ from 'jquery';

export const init = ($target) => {

	$target.find('.inp--count .btn--count').each(function() {
		handleCheckInputs($(this));

	});

	function handleCheckInputs($el) {
		var $this = $el,
			$parent = $this.closest('.inp--count'),
			$inpSub = $parent.find('.inp__preppend .btn--count'),
			$inpAdd = $parent.find('.inp__append .btn--count'),
			$input = $parent.find('.inp__text--count'),
			val = $input.val(),
			min = $input.attr('min'),
			max = $input.attr('max');

		if (val == min) {
			$inpSub.addClass('is-disabled');
		} else if (val == max) {
			// $inpAdd.addClass('is-disabled'); // nastaven popup
		} else {
			$inpSub.removeClass('is-disabled');
			$inpAdd.removeClass('is-disabled');
		}
	}

	$(document)
		.on('change', '.inp__text--count', function(e) {
			handleCheckInputs($(e.currentTarget));
		});
};
