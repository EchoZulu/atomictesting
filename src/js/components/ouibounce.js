import $ from 'jquery';
import ouibounce from 'ouibounce';

export const init = () => {

	ouibounce(document.getElementById('ouibounce-modal'), { callback: function() {
		var now = new Date(),
			cookieName = 'exit_popup';

		if (!readCookie(cookieName) && ($('#popup-exit').length > 0) ) {
			$.fancybox.open({
				src: '#popup-exit',
				type: 'inline',
				afterLoad: function (instance, current) {
					current.$content.trigger('contentload');
				},
			});

			now.setTime(now.getTime() + 24 * 60 * 60);
			document.cookie = cookieName + '=1;expires='+now.toUTCString()+';path=/';
		}
	}});

	function readCookie(name) {
		var nameEQ = name + '=';
		var ca = document.cookie.split(';');
		for (var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	}
};
