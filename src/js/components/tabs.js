import $ from 'jquery';
import { MQ } from '../tools/MQ';

export const init = ($target) => {

	$target.find('.js-tabs__item.is-active').each(function() {
		var $item = $(this),
			$parent = $item.closest('.js-tabs'),
			$underline = $parent.find('.b-tabs__underline');

		// onload arrangement
		if ( MQ('lgUp') ) {
			$underline.css({
				'left': $item.position().left + parseInt($item.css('marginLeft')) - (parseInt($item.find('.js-tabs__link').css('paddingLeft'))/ 2),
				'width': $item.outerWidth(),
			});
			$underline.css({
				'display': 'block',
			});

		} else if ( MQ('lgDown') ) {
			$underline.css({
				'display': 'none',
			});
		}
	});

	$(window).resize(function() {
		var $tabs = $target.find('.js-tabs');

		$tabs.each(function() {
			var $tab = $(this),
				$item = $tab.find('.js-tabs__item'),
				$underline = $tab.find('.b-tabs__underline');

			if ( $underline.length > 0  ) {
				underlineArrange($item, $underline);
			}
		});
	});

	$(document)
		.on('click', '.js-tabs__link', function(e) {
			var $this = $(this),
				$parent = $this.closest('.js-tabs'),
				$items = $parent.find('.js-tabs__item'),
				$tabs = $parent.find('.js-tabs__fragment'),
				$underline = $parent.find('.b-tabs__underline'),
				scroll = $this.data('scroll-up') ? true : false;

			console.log(scroll);
			$items.removeClass('is-active');
			$tabs.removeClass('is-active');

			if (typeof $parent.data('toggle-content') !== 'undefined') {
				scroll ? '' : e.preventDefault();
				$parent.find('.js-tabs__link[href="'+$this.attr('href')+'"]').closest('.js-tabs__item').each(function() {
					$(this).addClass('is-active');
				});
				$tabs.filter( $this.attr('href') ).addClass('is-active');
			} else {
				$this.closest('.js-tabs__item').addClass('is-active');
			}
			underlineArrange($items.filter('.is-active'), $underline);
		});

	function underlineArrange($item, $underline) {
		if ( MQ('lgUp') ) {
			$underline.animate({
				'left': $item.position().left + parseInt($item.css('marginLeft')) - (parseInt($item.find('.js-tabs__link').css('paddingLeft'))/ 2),
				'width': $item.outerWidth(),
			}, 300);
			$underline.animate({
				'display': 'block',
			});

		} else if ( MQ('lgDown') ) {
			$underline.css({
				'display': 'none',
			});
		}
	}



};
