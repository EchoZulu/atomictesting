import $ from 'jquery';

export const init = ($target) => {
	const dataSelectorFacebook = 'facebook-share';

	let fbShareWindowReference = null;

	init();

	function init() {
		let linkFacebok = $('a[href][data-' + dataSelectorFacebook + ']');
		linkFacebok.on('click', shareFacebook);
	}

	function shareFacebook(event) {
		if (fbShareWindowReference == null || fbShareWindowReference.closed) {
			let $link = $(event.currentTarget);
			let href = $link.attr('href');
			let popupWidth = 560;
			let popupHeight = 450;
			let popupLeft = window.outerWidth / 2 - popupWidth / 2;
			let popupTop = window.outerHeight / 2 - popupHeight / 2;

			fbShareWindowReference = window.open(href, null, `
				width=${popupWidth},
				height=${popupHeight},
				left=${popupLeft},
				top=${popupTop},
				menubar=no,
				location=yes,
				resizable=yes,
				scrollbars=yes,
				personalbar=false,
				status=false
			`);
		} else {
			fbShareWindowReference.focus();
		}
		event.preventDefault();
	}

};
