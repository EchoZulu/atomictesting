import $ from 'jquery';

export const init = ($target) => {

	$target.find('.js-antispam .inp__text').each(function() {
		// antispam for form
		$(this).attr('value', $(this).closest('.js-antispam').find('label strong').text());
	});
};
