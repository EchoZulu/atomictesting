import $ from 'jquery';
// import buildResponse from '../class/buildResponse';
const log = console.log;


export const init = () => {
    $(document)
        .on('change', '.f-ajax-transport-payment', function (event) {
            var $this = $(this);
            $this.data('ajax-action', $this.attr('action') + '?refresh=1');
            $this.addClass('js-ajax-form');
            $this.submit();
            $this.removeClass('js-ajax-form');
            $this.data('ajax-action', '');
        })
};
