import $ from 'jquery';
import throttle from 'lodash.throttle';
import { MQ } from '../tools/MQ';

export const init = ($target) => {

	var t = 300;


	$(document)
		.on('click', '.js-mobile-tab__link', function(e) {
			e.preventDefault();

			var $this = $(this),
				$parent = $this.closest('.js-mobile-tab'),
				$item = $this.closest('.js-mobile-tab__item'),
				$content = $item.find('.js-mobile-tab__content');

			if ( MQ('mdDown') || $parent.data('tab-always') ) {
				if ( $item.hasClass('is-open') ) {
					$content.slideUp(t);
					$item.removeClass('is-open');
				}
				else {
					$content.slideDown(t);
					$item.addClass('is-open');
				}

				// close other opened items
				$parent.find('.js-mobile-tab__item').not($item).find('.js-mobile-tab__content').slideUp(t, function() {
					$(this).closest('.js-mobile-tab__item').not($item).removeClass('is-open');
				});
			}
		})
		.on('click', '.js-mobile-tab__open', function() {
			var $this = $(this),
				href = $this.attr('href');
			open( href );
		});

	const open = (href) => {
		var $item = $target.find(href),
			$content = $item.find('.js-mobile-tab__content'),
			$parent = $item.closest('.js-mobile-tab');

		if ( !$item.hasClass('is-open') ) {
			$content.slideDown(t);
			$item.addClass('is-open');
		}

		$parent.find('.js-mobile-tab__item').not($item).find('.js-mobile-tab__content').slideUp(t, function() {
			$(this).closest('.js-mobile-tab__item').not($item).removeClass('is-open');
		});

	};

	$(window)
		.on('resize', throttle(function(e) {
			if ( MQ('smUp') ) {
				var $content = $('.js-mobile-tab__content');

				$content.removeAttr('style');
			}

		}, 100, {
			trailing: true,
		}));

};
