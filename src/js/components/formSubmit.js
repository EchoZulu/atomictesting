import $ from 'jquery';

export const init = ($target) => {

	const handleFormSubmit = (e) => {
		var $this = $(e.currentTarget),
			target = $this.data('submit-target');

		$target.find(target).submit();
	};

	$(document)
		.on('click', '.js-form-submit', handleFormSubmit);

};
