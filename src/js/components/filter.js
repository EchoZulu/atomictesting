import $ from 'jquery';
import { MQ } from '../tools/MQ';

export const init = ($target) => {

	$target
		.on('click', '.js-toggle-categories', function(e) {
			e.preventDefault();

			$(this).toggleClass('is-active');
			$target.find('.c-categories__item[data-hidden]').toggleClass('is-hidden');
		})
		.on('click', '.js-filter-checkbox', function(e) {
			e.preventDefault();
			var $this = $(this),
				$inp = $this.closest('.f-filter__item').find('input');

			$inp.is(':checked') ? $inp.prop('checked', false) : $inp.prop('checked', true);

			$inp.trigger('change');
		})
		// submit
		.on('submit', '.f-filter', function(e) {
			e.preventDefault();

			var $form = $(this);
			var data = {};

			$form.serializeArray().forEach(function(input) {
				// hodnoty ktere jsou stejne jako default neposilat na filtraci
				if ($('[name="' + input.name + '"]').data('default') != input.value) {
					data[input.name] = input.value;
				}
			});

			ajaxFilter($.param(data), $form.attr('action').split('?')[0], $form);
		})
		// links
		.on('click', '.c-products .b-paging__link, .c-products .b-paging-row .btn, .f-sort__link, .b-filters__remove, .inp-custom-select__link, .imagesPager .b-paging__link, .imagesPager .b-paging-row__btn-link', function (e) {
			e.preventDefault();

			var $this = $(this);
			if ($(e.currentTarget).hasClass('b-paging__link') &&
				$(e.currentTarget).closest('#snippet--productsPagerBottom').length &&
				$target.find('.c-products__wrap').length
			) {
				$('body, html').animate({
					scrollTop: $target.find('.c-products__wrap').offset().top - $target.find('.header').height() - 20,
				}, 300);
			}
			ajaxFilter($this.attr('href').split('?')[1], $this.attr('href').split('?')[0], $this); // $this.closest('.paging').length
		})
		// Change
		.on('change', '.f-filter input, .f-filter select', function() {
			if ( MQ('lgUp') ) {
				$(this).closest('.f-filter').submit();
			}
		});

	var request = null;

	// ajaxFilter
	var ajaxFilter = function (data, formAction, $target, isPopstate) {

		if (request != null) {
			request.abort();
		}

		request = $.ajax({
			url: formAction,
			method: 'get',
			data: data,
			beforeSend: function() {
				$('body').addClass('page-loading');
			},
		})
		.done(function(payload) {
			success(payload);

			$('body').removeClass('page-loading');

			var undefined;


			// scroll na začátek výsledků
			if ( MQ('lgDown') ) {
				if ( !$target.hasClass('b-paging-row__btn-link') ) {
					var filterOffsetTop = $('.f-filter').length ? $('.f-filter').offset().top : 0;

					if ($(document).scrollTop() > filterOffsetTop) {
						$('body, html').animate({
							scrollTop: filterOffsetTop - 20 - $('.header').height(),
						}, 500);
					}
				}
			}

			if (!isPopstate) {
				if ( payload.newUrl != undefined) {
					history.pushState({
						data: data,
						isFilter: true,
						formAction: formAction,
					}, '', payload.newUrl );
				}
				// else {
				// 	console.log('link');
				// }
			}
		});
	};

	var success = function(payload) {
		if ( !payload ) {
			return;
		}
		if ( payload.redirect ) {
			window.location.href = payload.redirect;
			return;
		}
		if ( payload.refresh ) {
			window.location.reload();
			return;
		}
		if ( payload.snippets ) {
			for (var i in payload.snippets) {
				var $el = $('#' + i);

				if ($el.is('[data-ajax-append]')) {
					appendSnippet($el, payload.snippets[i]);
				} else {
					updateSnippet($el, payload.snippets[i]);
				}
			}
		}
	};

	var updateSnippet = function($el, html) {
		$el.html(html);
		$el.trigger('contentload');
	};

	var appendSnippet = function($el, html) {
		$el.append('<div class="appended-html">' + html + '</div>');
		$el.find(' .appended-html').trigger('contentload').find('> *').unwrap();
	};

	// Popstate
	$(window)
		.on('popstate', function(event) {
			var state = event.originalEvent.state;

			if (state != null && state.isFilter ) {
				ajaxFilter(state.data, state.formAction, $(document), true);
			}
		});

	// Default state
	$('.f-filter')
		.each(function() {
			history.replaceState({
				data: window.location.search,
				isFilter: true,
				formAction: window.location.pathname,
			}, '', window.location.href);
		});
};
