import $ from 'jquery';

export const init = () => {

	$(document)
		.on('change changeInit', '.js-transport__inp', function(e) {
			var $this = $(this),
				$item = $('.js-payment__inp'),
				$selected = $($this.data('selector'));


			if ( (e.type != 'changeInit') || (e.type == 'changeInit' && $this.is(':checked')) ) {
				$item
					.prop('disabled', true)
					.closest('li').addClass('is-disabled');

				$item.filter($selected)
					.prop('disabled', false)
					.closest('li').removeClass('is-disabled');

				$item.filter(':disabled:checked')
					.prop('checked', false)
					.closest('li').removeClass('is-checked');
			}
		});

	// trigger
	$('.js-transport__inp').trigger('changeInit');
};
