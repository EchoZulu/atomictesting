import $ from 'jquery';
export const init = () => {

	$(document).on('click', '.js-radio-deselect input[type="radio"]', function() {
		var $this = $(this),
			$radios = $this.closest('.js-radio-deselect').find('input[type="radio"]');

		if ($this.hasClass('is-checked')) {
			$this.removeClass('is-checked');
			$this.prop('checked', false);
			$this.trigger('change');
		} else {
			$radios.each(function() {
				$(this).prop('checked', false);
				$(this).removeClass('is-checked');
			});
			$this.prop('checked', true);
			$this.addClass('is-checked');
		}
	});
};

