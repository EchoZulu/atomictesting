import $ from 'jquery';
import Headroom from 'headroom.js';


export const init = () => {
	const $document = $(document);
	const $header = $('[data-header]');

	const onUnpin = () => $document.trigger('headroomUnpin');

	const OPTIONS = {
		tolerance: {
			down : 20,
			up : 20,
		},
		offset : 190,
		onUnpin,
	};
	const headroom = new Headroom($header[0], OPTIONS);


	headroom.init();
};
