import $ from 'jquery';
import Cookies from 'cookies-js';

export const init = ($target) => {

	$(document)
		.on('click', '.b-cookie .btn', function(e) {
			e.preventDefault();

			var date = new Date();
			date.setFullYear(date.getFullYear() + 10);
			document.cookie = 'eu-cookies=1; path=/; expires=' + date.toGMTString();

			$('.b-cookie').fadeOut();
		})
		.on('click', 'a.js-tabs__link', function(e) {

			const tabName = $(e.target).closest('li').data('tabname');

			const in30Minutes = new Date(new Date().getTime() + 30 * 60 * 1000);
			// console.log(tabName);
			Cookies.set('activeTabName', tabName, {
				expires: in30Minutes,
			});
		})
		.on('click', '.js-filter__link', function(e) {
			e.preventDefault();


			const $parent = $(e.target).closest('.f-filter__group');
			const isOpen = $parent.hasClass('is-open');
			const filterName = $parent.data('internaluid') + $parent.data('objectid');

			const inFifteenMinutes = new Date(new Date().getTime() + 15 * 60 * 1000);

			Cookies.set('isOpen' + filterName, (isOpen)? 0: 1, {
				expires: inFifteenMinutes,
			});
		});



};
