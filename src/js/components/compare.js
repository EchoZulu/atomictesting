import $ from 'jquery';
import 'slick-carousel';
import { MQ } from '../tools/MQ';
import '../static/jquery-ui.min.js';
// import 'sortablejs';

export const init = ($target) => {

	const selectors = {
		carousel: {
			SLIDER: 'slick-slider',
			SLIDE: 'slick-slide',
			TRACK: 'slick-track',
			ACTIVE: 'slick-active',
			MOVE: 'slick-move',
			REMOVE: 'slick-remove',
		},
		PARENT: 'c-compare',
		LIST: 'c-compare__list',
		PREV: 'c-compare__prev',
		NEXT: 'c-compare__next',
		HANGER: '[data-toggle-hanger]',
		COMPARE: 'b-compare',
		LINK_COPY: 'b-compare__link--copy',
	};

	const classes = {
		IS_OPEN: 'is-open',
	};

	$target.find('.c-compare').each(function () {
		const $this = $(this);
		const $scroll = $this.find('.'+selectors.LIST);
		const $prev = $this.find('.'+selectors.PREV);
		const $next = $this.find('.'+selectors.NEXT);

		$this.data(selectors.carousel.ACTIVE, false);

		$(window).on('resize.compare', function() {
			const $track = $('.'+selectors.LIST).find('.'+selectors.carousel.TRACK);
			if (MQ('lgUp') && !$this.data(selectors.carousel.ACTIVE)) {
				$scroll.slick({
					arrow: true,
					dots: false,
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: false,
					prevArrow: $prev,
					nextArrow: $next,
					draggable: false,
					responsive: [
						{
							breakpoint: 1400,
							settings: {
								slidesToShow: 2,
								draggable: false,
							},
						},
					],
				});
				$this.data(selectors.carousel.ACTIVE, true);
				$track.sortable('enable');
			}
			else if (MQ('lgDown') && $this.data(selectors.carousel.ACTIVE)) {
				$this.find('.'+selectors.carousel.SLIDER).each(function () {
					var $that = $(this);
					$that.slick('unslick');
				});
				$this.data(selectors.carousel.ACTIVE, false);
				$track.sortable('disable');
			}
		});

	});

	const handleRemove = (e) => {
		e.preventDefault();

		const $this = $(e.currentTarget);
		const $slider = $this.closest('.'+selectors.carousel.SLIDER);
		const $slide = $this.closest('.'+selectors.carousel.SLIDE);

		$slider.slick('slickRemove', $('.'+selectors.carousel.SLIDE).index($slide));
	};

	const handleCarouselInit = () => {
		const $track = $('.'+selectors.LIST).find('.'+selectors.carousel.TRACK);

		$track.sortable({
			axis: 'x',
			revert: true,
			scroll: false,
			placeholder: 'sortable-placeholder',
			cursor: 'move',
			start:  function(e, ui) {
				var $this = $(e.currentTarget),
					matrix = $this.css('transform').match(/matrix(?:(3d)\(-{0,1}\d+(?:, -{0,1}\d+)*(?:, (-{0,1}\d+))(?:, (-{0,1}\d+))(?:, (-{0,1}\d+)), -{0,1}\d+\)|\(-{0,1}\d+(?:, -{0,1}\d+)*(?:, (-{0,1}\d+))(?:, (-{0,1}\d+))\))/),
					positiveMatrixX = matrix[5] * (-1);
				ui.helper.css('transform', 'translate3d('+positiveMatrixX+'px, 0px, 0px)');
			},
			stop: function(e, ui) {
				ui.item.css('transform', 'translate3d(0px, 0px, 0px)');
			},
		});

		if (MQ('lgDown')) {
			$track.sortable('disable');
		}

	};

	const handleToggleHanger = (e) => {
		e.preventDefault();
		$(e.currentTarget).closest('.'+selectors.COMPARE).toggleClass(classes.IS_OPEN);
	};

	$(document)
		.on('click', selectors.HANGER, handleToggleHanger)
		.on('click', '.'+selectors.carousel.REMOVE, handleRemove)
		.on('init', '.'+selectors.carousel.SLIDER, handleCarouselInit);

	$(window).trigger('resize.compare');

};
