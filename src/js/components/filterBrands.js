import $ from 'jquery';

export const init = () => {

	$(document)
		.on('click', '.js-filter-brands__link', function(e) {
			e.preventDefault();
			var $this = $(this),
				letter = $this.text().trim().toUpperCase(),
				$parent = $this.closest('.js-filter-brands'),
				$items = $parent.find('.js-filter-brands__item');

			$items.addClass('u-hide').filter(function() {
				if ( $this.data('show-best-sellers') == true ) {
					return $(this).data('best-seller') == true;
				} else {
					return $(this).find('.js-filter-brands__name').text().trim().slice(0, 1).toUpperCase() == letter;
				}
			}).removeClass('u-hide');

		});

};
