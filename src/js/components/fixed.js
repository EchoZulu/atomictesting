import $ from 'jquery';
import { MQ } from '../tools/MQ';

export const init = ($target) => {
	// fixed bar
	$target.find('.js-fixed').each(function() {

		var $this  = $(this),
			$fixedWrap = $this.find('.js-fixed__wrap'),
			$header = $target.find('.headroom'),
			$fixedTarget = $this.find('.js-fixed__target');

		if ( MQ('lgUp') ) {
			$fixedTarget.width( $fixedTarget.parent().innerWidth() );
			$fixedWrap.height( $fixedWrap.outerHeight() );
		}

		$(window).on('scroll', function() {
			if ( MQ('lgUp') ) {
				if ( $this.outerHeight() > $fixedTarget.outerHeight() ) {
					// should be fixed?
					if ($(document).scrollTop() > ($this.offset().top - $header.height()) && $target.find('.headroom--pinned, .headroom--top').length || $(document).scrollTop() > $this.offset().top ) {
						$fixedTarget.addClass('is-fixed');

						// fixed position
						if ($(document).scrollTop() + $fixedTarget.height() > $this.offset().top + $this.outerHeight() - $header.height() && $target.find('.headroom--pinned').length ||
								$(document).scrollTop() + $fixedTarget.height() > $this.offset().top + $this.outerHeight() ) {
							$fixedTarget.addClass('is-fixed-bottom');
						}
						else {
							$fixedTarget.removeClass('is-fixed-bottom');
						}
					}
					else {
						$fixedTarget.removeClass('is-fixed');
					}
				}
			}
		});

		$(window).on('resize', function() {
			if ( MQ('lgUp') ) {
				$fixedTarget.width( $fixedTarget.parent().innerWidth() );
				$fixedWrap.height( $fixedTarget.outerHeight() );
				$fixedWrap.height( $fixedWrap.outerHeight() );
			} else {
				$fixedTarget.removeAttr( 'style' );
				$fixedWrap.removeAttr( 'style' );
			}
		});



	});

};
