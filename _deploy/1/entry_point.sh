#!/bin/bash
# Required globals:
#   SERVER
#   USER
#   REMOTE_PATH
#
# Optional globals:
#   SSH_KEY
#   EXTRA_ARGS
#   DEBUG





source $(dirname $0)/common.sh


run_pipe() {

	changes_file=$BITBUCKET_CLONE_DIR/pipe/git_changes.log
	remote_hash_file=$BITBUCKET_CLONE_DIR/pipe/remote_hash.log

	mkdir "$BITBUCKET_CLONE_DIR/pipe"
	touch $remote_hash_file
	touch $changes_file
	info $remote_hash_file

	info "Checking remote ssh for last commit hash ${SERVER}:${REMOTE_PATH}..."
	info "${USER}@${SERVER} in path ${REMOTE_PATH}"
	set +e

	ssh ${USER}@${SERVER} -t "cd ${REMOTE_PATH}; cat .git-ftp.log"
	ssh ${USER}@${SERVER} -t "cd ${REMOTE_PATH}; cat .git-ftp.log" > $remote_hash_file

 	remote_version=$(cat $remote_hash_file)
 	info $remote_version

 	if [[ "${remote_version}" ]]; then
    	git --no-pager diff --text --name-only $remote_version > $changes_file
      	success "Creating {$changes_file}"
    	debug $(cat $changes_file)

	fi
	set -e

	exit 0
}


validate
enable_debug
setup_ssh_dir
run_pipe


# conect to remote and copy gitftp state

# git diff



echo "tady bude list" > /changes.report
# ssh host 'cat /path/on/remote' > /path/on/local





