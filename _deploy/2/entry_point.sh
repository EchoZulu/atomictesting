#!/bin/bash
# Required globals:
#   SERVER
#   USER
#   REMOTE_PATH
#
# Optional globals:
#   SSH_KEY
#   EXTRA_ARGS
#   DEBUG



source $(dirname $0)/common.sh


run_pipe() {
  debug "run pipeline"
  changes_file=$BITBUCKET_CLONE_DIR/pipe/git_changes.log

  runGulpMin=false

  if test -f "$changes_file"; then
    debug "$changes_file exist"
    result=$(cat $changes_file | grep -- 'src/\|package\.json\|package-lock\.json')

    if [ ! -z "$result" ] ; then
      debug "$changes_file contain changes"
      runGulpMin=true
    fi
  else
    debug "$changes_file NOT exist"
    runGulpMin=true
  fi

  if [ ! -z "$runGulpMin" ] ; then
    info "copy www/s to remote"
    #build
    ls $BITBUCKET_CLONE_DIR
#    cd $BITBUCKET_CLONE_DIR
#    chmod 777 node_module
    npm i
    npm i gulp -g
    gulp min
  fi

  exit 0
}


validate
enable_debug
setup_ssh_dir
run_pipe





