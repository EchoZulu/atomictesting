#!/bin/bash
# Required globals:
#   SERVER
#   USER
#   REMOTE_PATH
#
# Optional globals:
#   SSH_KEY
#   EXTRA_ARGS
#   DEBUG


source $(dirname $0)/common.sh


run_pipe() {
	changes_file=/tmp_pipe/git_changes.log

  runComposer=false

  if test -f "$changes_file"; then
    debug "$changes_file exist"
    result=$(cat $changes_file | grep -- 'composer\.lock\|composer\.json')

    if [ ! -z "$result" ] ; then
      debug "$changes_file contain changes of composer.json"
      runComposer=true
    fi
  else
    debug "$changes_file NOT exist"
    runComposer=true
  fi

  if [ ! -z "$runComposer" ] ; then
    info "copy VENDOR to remote"
    composer install --ignore-platform-reqs
  fi

  exit 0
}


validate
enable_debug
setup_ssh_dir
run_pipe





