#!/bin/bash
remote_path="/chroot/u13623/srv/app"
user="u13623"
ssh_key="$HOME/.ssh/id_rsa"
host="sftp://superkoderi5.cz-hosting.com"


RED='\033[0;31m'
YELLOW='\033[0;33m'
NC='\033[0m'

git_ftp_remote_version=""
git_ftp_up_to_date=""

redText() {
    echo "${RED}$1${NC}"
}


yellowText() {
    echo "${YELLOW}$1${NC}"
}

greenText() {
    echo "\e[31m$1${NC}"
}

getRemoteVersion() {
    #git-ftp push --auto-init  --remote-root  $1 -u $2 --key $3 $4 -f -D | read neco
    git-ftp push --auto-init --remote-root  $1 -u $2 --key $3 $4 -f -D > /tmp/git_ftp_remote_status
    git_ftp_remote_version=$(cat /tmp/git_ftp_remote_status | grep "Last deployment changed from"  | cut -d' ' -f5 | cut -d'.' -f1)
    git_ftp_up_to_date=$(cat /tmp/git_ftp_remote_status | grep "Everything up-to-date")
    rm /tmp/git_ftp_remote_status
}

copyDirectoryToRemote() {

    localDirectory=$4
    remoteDirectory=$5

    newDir="${remoteDirectory}_NEW"
    oldDir="${remoteDirectory}_OLD"

    lftp -u $1, $2 -e "rm -rf ${newDir} ; rm -rf ${oldDir} ; mirror -R --parallel=10 ${localDirectory} ${newDir} ; mv ${remoteDirectory} ${oldDir} ; mv ${newDir} ${remoteDirectory} ; rm -rf ${oldDir} ; exit"
}


gitFtpPush() {
    git-ftp push --auto-init  --remote-root  $1 -u $2 --key $3 $4 -f
}


gulpMinTask() {

    git --no-pager diff --text --name-only $1 > /tmp/git_changes
    result=$(cat /tmp/git_changes | grep -- 'src/\|package\.json\|package-lock\.json')
    yellowText $result

    if [ ! -z "$result" ] || [ -z "$git_ftp_remote_version" ] ; then
        yellowText "gulp min"
        yellowText "copy www/static to remote"
        copyDirectoryToRemote $user $host $ssh_key "www/static" "${remote_path}/www/static"
    else
        # result is empty skip task
        yellowText "without gulp min"
    fi
}

copyVendor() {

    git --no-pager diff --text --name-only $1 > /tmp/git_changes
    result=$(cat /tmp/git_changes | grep -- 'composer\.lock\|composer\.json')
    yellowText $result

    if [ ! -z "$result" ] || [ -z "$git_ftp_remote_version" ] ; then
        yellowText "copy to remote"
        copyDirectoryToRemote $user $host $ssh_key "vendor" "${remote_path}/vendor"
    else
        # result is empty skip task
        yellowText "without vendor copy"
    fi
}

git status
# fill git_ftp_remote_version variable
getRemoteVersion $remote_path $user $ssh_key $host
if [ -z "$git_ftp_up_to_date" ] ; then
    yellowText $git_ftp_remote_version

    #tasks
    gulpMinTask $git_ftp_remote_version
    copyVendor $git_ftp_remote_version
    gitFtpPush $remote_path $user $ssh_key $host
else
    yellowText "Without changes"
fi


