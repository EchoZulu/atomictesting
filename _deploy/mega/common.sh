
# Begin Standard 'imports'
set -e
set -o pipefail


grey="\\e[37m"
blue="\\e[36m"
red="\\e[31m"
green="\\e[32m"
reset="\\e[0m"

# Logging, loosely based on http://www.ludovicocaldara.net/dba/bash-tips-4-use-logging-levels/
info() { echo -e "${blue}INFO: $*${reset}"; }
error() { echo -e "${red}ERROR: $*${reset}"; }
debug() { if [[ "${DEBUG}" == "true" ]]; then echo -e "${grey}DEBUG: $*${reset}"; fi }
success() { echo -e "${green}✔ $*${reset}"; }
fail() { echo -e "${red}✖ $*${reset}"; }

# End standard 'imports'

info "STARTED"

SFTP_DEBUG_ARGS=
## Enable debug mode.
enable_debug() {
  if [[ "${DEBUG}" == "true" ]]; then
    info "Enabling debug mode."
    set -x
    SFTP_DEBUG_ARGS="-v"
  fi
}

validate() {
  # mandatory parameters
  : SERVER=${SERVER:?'SERVER variable missing.'}
  : USER=${USER:?'USER variable missing.'}
  : REMOTE_PATH=${REMOTE_PATH:?'REMOTE_PATH variable missing.'}
}

setup_ssh_dir() {
  INJECTED_SSH_CONFIG_DIR="/opt/atlassian/pipelines/agent/ssh"
  # The default ssh key with open perms readable by alt uids
  IDENTITY_FILE="${INJECTED_SSH_CONFIG_DIR}/id_rsa_tmp"
  # The default known_hosts file
  KNOWN_HOSTS_FILE="${INJECTED_SSH_CONFIG_DIR}/known_hosts"

  mkdir -p ~/.ssh || debug "adding ssh keys to existing ~/.ssh"
  touch ~/.ssh/authorized_keys

  if [[ -z "${PASSWORD}" ]]; then
    # If given, use SSH_KEY, otherwise check if the default is configured and use it
    if [ "${SSH_KEY}" != "" ]; then
       debug "Using passed SSH_KEY"
       (umask  077 ; echo ${SSH_KEY} | base64 -d > ~/.ssh/pipelines_id)
    elif [ ! -f ${IDENTITY_FILE} ]; then
       error "No default SSH key configured in Pipelines."
       exit 1
    else
       debug "Using default ssh key"
       cp ${IDENTITY_FILE} ~/.ssh/pipelines_id
    fi
  fi

  if [ ! -f ${KNOWN_HOSTS_FILE} ]; then
      error "No SSH known_hosts configured in Pipelines."
      exit 2
  fi

  cat ${KNOWN_HOSTS_FILE} >> ~/.ssh/known_hosts
  if [ -f ~/.ssh/config ]; then
      debug "Appending to existing ~/.ssh/config file"
  fi

  if [[ -z "${PASSWORD}" ]]; then
    echo "IdentityFile ~/.ssh/pipelines_id" >> ~/.ssh/config
  fi
  chmod -R go-rwx ~/.ssh/
}