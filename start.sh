#delete cache
echo "DELETING nette cache"
find temp/cache ! -name '.gitkeep' -type f -exec rm -f {} +

export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/opt/X11/lib/pkgconfig
yarn
gulp
