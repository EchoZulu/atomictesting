var config = require('./helpers/getConfig.js');
var isProduction = require('./helpers/isProduction.js');

var gulp = require('gulp');
var path = require('path');
var gutil = require('gulp-util');
var notify = require('gulp-notify');
var webpack = require('webpack');

const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

var isMinwatch = function() {
	return gutil.env._[0] === 'minwatch';
};


gulp.task('webpack', function(callback) {
	var isReady = false;
	var settings = {
		resolve: {
			extensions: ['.js', '.json', '.coffee', '.jsx'],
			modules: [
				path.join(__dirname, '../node_modules'),
				path.join(__dirname, '../bower_components'),
			],
		},
		entry: {
			app: './' + config.src.scripts + 'app',
		},
		output: {
			path: path.join(__dirname, '../' + config.dest.scripts),
			filename: '[name].js',
			publicPath: '/static/js/',
			chunkFilename: '[name].chunk.js',
		},
		module: {
			rules: [
				{
					test: /\.coffee$/,
					loader: 'coffee-loader',
				}, {
					test: /\.js(x)?$/,
					exclude: /node_modules|bower_components/,
					loader: 'babel-loader',
				}, {
					test: /packery|scrollmagic|gsap/,
					loader: 'imports-loader?define=>false&this=>window',
				},
			],
		},
		plugins: [
		// 	new BundleAnalyzerPlugin(),
		],
		profile: true,
		watch: !isProduction() || isMinwatch(),
		watchOptions: {
			ignored: /node_modules|bower_components/,
		},
		devtool: isProduction() ? false : 'source-map',
		externals: {
			'jquery': 'jQuery',
		},
	};

	if (isProduction()) {
		settings.plugins.push(new webpack.optimize.UglifyJsPlugin({
			compress: {
				screw_ie8: true,
				warnings: false,
				properties: true,
				sequences: true,
				dead_code: true,
				drop_debugger: true,
				unsafe: false,
				conditionals: true,
				evaluate: true,
				booleans: true,
				loops: true,
				unused: true,
				if_return: true,
				join_vars: true,
				cascade: true,
				hoist_vars: false,
				hoist_funs: true,
				drop_console: true,
			},
			comments: false,
		}));

		settings.plugins.push(new webpack.optimize.ModuleConcatenationPlugin());
	}

	var onError = notify.onError(function(error) {
		return {
			title: 'JS error!',
			message: error,
			sound: 'Beep',
		};
	});

	var bundle = webpack(settings, function(error, stats) {
		var jsonStats = stats.toJson();
		var errors = jsonStats.errors;
		var warnings = jsonStats.warnings;

		if (error) {
			onError(error);
		} else if (errors.length > 0) {
			onError(errors.toString());
		} else if (warnings.length > 0) {
			onError(warnings.toString());
		} else {
			gutil.log('[webpack]', stats.toString(config.webpack.stats));
		}

		if (!isReady) {
			callback();
		}

		return isReady = true;
	});

	return bundle;
});
