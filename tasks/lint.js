var gulp = require('gulp');
var runSequence = require('run-sequence');


gulp.task('lint', function(callback) {
	var sequence = runSequence([
		'stylint',
		'eslint',
	],
		callback,
	);

	return sequence;
});
