var config = require('./helpers/getConfig.js');

var gulp = require('gulp');
var	eslint = require('gulp-eslint');


gulp.task('eslint', function () {
	var stream = gulp.src([
		'**/*.js',
	], {
		cwd: config.src.scripts,
	});

	stream
		.pipe(eslint())
		.pipe(eslint.format());

	return stream;
});
